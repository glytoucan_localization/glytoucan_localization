create database glytoucan_localization default character set utf8;

use glytoucan_localization;

create table users(id int unsigned primary key auto_increment, name varchar(255) unique not null, password varchar(255) not null, registered_date timestamp null default CURRENT_TIMESTAMP, is_admin int(1) default 0) engine=InnoDB default character set utf8;

insert into users(name, password, is_admin) values('administrator', 'eea9e76ef75508acddd1b5095839e7ce38216071', 1), ('fujita', 'eea9e76ef75508acddd1b5095839e7ce38216071', 0), ('takahashi', 'eea9e76ef75508acddd1b5095839e7ce38216071', 0);

create table languages(id int unsigned primary key auto_increment, name varchar(255) unique not null) engine=InnoDB default character set utf8;
insert into languages(name) values('English'), ('日本語'), ('中文(简体)'), ('中文(繁體)');

create table places(id int unsigned primary key auto_increment, name varchar(255) unique not null) engine=InnoDB default character set utf8;
insert into places(name) values('Title'), ('TopTitle'), ('Top'), ('RightTitle'), ('Right'), ('LeftTitle'), ('Left'), ('BottomTitle'), ('Bottom'), ('BottomFigure'), ('FootTitle'), ('Foot'), ('FootBottom'), ('Register'), ('Error'), ('ErrorMessage'), ('Registered');

create table controller_names(id int unsigned primary key auto_increment, name varchar(255) unique not null) engine=InnoDB default character set utf8;
insert into controller_names(name) values('Facets'), ('Motifs'), ('Preferences'), ('Registries'), ('Stanzas'), ('Structures'), ('Users');

create table view_names(id int unsigned primary key auto_increment, cont_id int unsigned not null, name varchar(255) not null, unique(cont_id, name), constraint fk_view_1 foreign key(cont_id) references controller_names(id) on delete cascade on update cascade) engine=InnoDB default character set utf8;
insert into view_names(cont_id, name) values
(1, 'json/glycans_json'),
(1, 'json/glycans_json_template'),
(1, 'glycans_facet'),
(2, 'list_all'),
(2, 'motifs'),
(2, 'motifs_list'),
(2, 'motifs_search'),
(2, 'motifs_tag'),
(2, 'results'),
(2, 'search'),
(3, 'index'),
(4, 'complete'),
(4, 'confirmation'),
(4, 'create'),
(4, 'graphical'),
(4, 'index'),
(4, 'motif'),
(4, 'newMotif'),
(4, 'no_data'),
(4, 'result'),
(4, 'upload'),
(5, 'check'),
(5, 'check_form'),
(5, 'glycan'),
(5, 'glycans'),
(5, 'glycans_list'),
(5, 'index'),
(5, 'insert'),
(5, 'interaction'),
(5, 'no_data'),
(5, 'registry'),
(5, 'result'),
(5, 'search_by_substructure'),
(5, 'substructure'),
(6, 'check'),
(6, 'check_form'),
(6, 'composition_search'),
(6, 'exact'),
(6, 'glycans'),
(6, 'glycans_list'),
(6, 'graphical'),
(6, 'motif'),
(6, 'search_by_substructure'),
(6, 'search_comp'),
(6, 'search_exact'),
(6, 'search_results'),
(6, 'search_sub'),
(6, 'structure_search'),
(6, 'sub'),
(6, 'substructure'),
(7, 'complete'),
(7, 'confirmation'),
(7, 'editpassword'),
(7, 'emailpw'),
(7, 'emailpwform'),
(7, 'index'),
(7, 'list_all'),
(7, 'no_data'),
(7, 'profile'),
(7, 'recover'),
(7, 'recoveruser'),
(7, 'recoveruserform'),
(7, 'registry'),
(7, 'result'),
(7, 'up');

create table articles(id int unsigned primary key auto_increment, user_id int unsigned not null, lang_id int unsigned not null, view_id int unsigned not null, place_id int unsigned not null, num int unsigned not null, created_date timestamp not null default '0000-00-00 00:00:00', modified_date timestamp null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP, article longtext default null, constraint fk_art_1 foreign key(user_id) references users(id) on delete cascade on update cascade, constraint fk_art_2 foreign key(lang_id) references languages(id) on delete cascade on update cascade, constraint fk_art_3 foreign key(view_id) references view_names(id) on delete cascade on update cascade, constraint fk_art_4 foreign key(place_id) references places(id) on delete cascade on update cascade) engine=InnoDB default character set utf8;

create table navigation_menu_items(id int unsigned primary key auto_increment, name varchar(255) unique not null) engine=InnoDB default character set utf8;
insert into navigation_menu_items(name) values 
('navbar_homeLabel'),
('navbar_searchLabel'),
('navbar_viewAllLabel'),
('navbar_preferencesLabel'),
('navbar_signInLabel'),
('navbar_signUpLabel'),
('navbar_accessionNumberPlaceholder'),
('navbar_searchButton'),
('navbar_searchLabelItem_structureSearchLabel'),
('navbar_searchLabelItem_compositionSearchLabel'),
('navbar_searchLabelItem_motifSearchLabel'),
('navbar_searchLabelItem_graphicalSearchLabel'),
('navbar_viewAllLabelItem_motifListLabel'),
('navbar_viewAllLabelItem_glycanListLabel'),
('signIn_titleLabel'),
('signIn_userNameLabel'),
('signIn_passwordLabel'),
('signIn_emailPasswordButton'),
('signIn_recoverUserNameButton'),
('signIn_submitButton'),
('signIn_socialLoginText'),
('signUp_titleLabel'),
('signUp_userNameText'),
('signUp_userNameLabel'),
('signUp_passwordText'),
('signUp_passwordLabel'),
('signUp_confirmPasswordLabel'),
('signUp_fullNameLabel'),
('signUp_emailLabel'),
('signUp_affiliationLabel'),
('signUp_submitButton'),
('signUp_footerText');

create table navigation_menus(id int unsigned primary key auto_increment, user_id int unsigned not null, lang_id int unsigned not null, menu_item_id int unsigned not null, created_date timestamp not null default '0000-00-00 00:00:00', modified_date timestamp null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP, data longtext, constraint fk_nav_1 foreign key(user_id) references users(id) on delete cascade on update cascade, constraint fk_nav_2 foreign key(lang_id) references languages(id) on delete cascade on update cascade, constraint fk_nav_3 foreign key(menu_item_id) references navigation_menu_items(id) on delete cascade on update cascade) engine=InnoDB default character set utf8;

create table content_types(id int unsigned primary key auto_increment, name varchar(255) unique not null) engine=InnoDB default character set utf8;
insert into content_types(name) values('article'), ('navigation_menu');

create table change_logs(id int unsigned primary key auto_increment, date timestamp not null default '0000-00-00 00:00:00', user_id int unsigned not null, content_type_id int unsigned not null, target_id int unsigned not null, constraint fk_log_1 foreign key(user_id) references users(id) on delete cascade on update cascade, constraint fk_log_2 foreign key(content_type_id) references content_types(id) on delete cascade on update cascade) engine=InnoDB default character set utf8;
