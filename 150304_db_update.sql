alter table articles add column disabled tinyint(1) default 0;
alter table navigation_menus add column disabled tinyint(1) default 0;

insert into content_types(name) values('common');

create table common_items(
	id int unsigned primary key auto_increment, 
	name varchar(255) unique not null
) engine=InnoDB default character set utf8;

create table commons(
	id int unsigned primary key auto_increment, 
	user_id int unsigned not null, 
	lang_id int unsigned not null, 
	item_id int unsigned not null, 
	created_date timestamp not null default '0000-00-00 00:00:00', 
	modified_date timestamp null default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP, 
	data longtext, 
	disabled tinyint(1) default 0, 
	constraint fk_common_1 foreign key(user_id) references users(id) on delete cascade on update cascade, 
	constraint fk_common_2 foreign key(lang_id) references languages(id) on delete cascade on update cascade, 
	constraint fk_common_3 foreign key(item_id) references common_items(id) on delete cascade on update cascade
) engine=InnoDB default character set utf8;
