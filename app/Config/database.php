<?php

class DATABASE_CONFIG {

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'local.glytoucan.org',
		'login' => 'root',
		'password' => 'root',
		'database' => 'glytoucan_test',
		'prefix' => '',
		//'encoding' => 'utf8',
	);

	// MySQL用のデータベース設定
	public $mysql = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'local.glytoucan.org',
		'login' => 'root',
		'password' => 'root',
		'database' => 'glytoucan_localization',
		'prefix' => '',
		'encoding' => 'utf8',
	);
}
