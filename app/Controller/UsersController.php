<?php
class UsersController extends AppController{

	// 使用するモデルを指定
	public $uses = array(
		'User',
	);

	// 使用するコンポーネントを指定
	public $components = array(
	);

	// beforeFilterコールバック(各アクション実行前に実行)
	public function beforeFilter(){
		// ログイン無しでアクセスできるアクションを列挙する 引数なしで全アクションアクセス可能にする
		$this->Auth->allow('login');
		// 認証コンポーネントをViewで利用可能にしておく
		$this->set('auth', $this->Auth);
	}

	// ユーザーログインアクション
	public function login(){
		// 認証を行う
		if($this->request->isPost() || $this->request->isPut()){
			if($this->Auth->login()){
				$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
				return;
			}else{
				$this->Session->setFlash('Log In failed. Account name or password is incorrect.', 'flash_alert_danger', null, 'log_in_message');
				$this->Session->setFlash('failed.', null, null, 'log_in_status');
				$this->Session->setFlash($this->request->data['User']['name'], null, null, 'log_in_name');
			}
		}
		// indexにリダイレクト
		$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
	}

	// ユーザーログアウトアクション
	public function logout(){
		if($this->request->isPost() || $this->request->isPut()){
			//$this->Session->delete('login');
			if($this->Auth->logout()){
				$this->Session->destroy();
			}else{
				$this->Session->setFlash('failded', null, null, 'log_out_status');
				$this->Session->setFlash('Log Out failed.', 'flash_alert_danger', null, 'log_out_message');
			}
		}
		// indexにリダイレクト
		$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
	}

	// ユーザー登録アクション
	public function registration(){
		if($this->request->isPost() || $this->request->isPut()){
			if($this->Auth->user()['is_admin'] == 1){
				if(!empty($this->request->data)){
					try{
						if($this->User->save($this->request->data)){
							$this->Session->setFlash('registered', null, null, 'registered');
							// indexにリダイレクト
							$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
							return;
						}else{
							$this->Session->setFlash('failed', null, null, 'registration_status');
							$this->Session->setFlash('User registration failed. Account name or password is not correct', 'flash_alert_danger', null, 'registration_message');
						}
					}catch(Exception $e){
						// 例外対処
						$this->Session->setFlash('failed', null, null, 'registration_status');
						$this->Session->setFlash('User registration failed. Account name or password is not correct', 'flash_alert_danger', null, 'registration_message');
					}
				}
			}
		}
		// indexにリダイレクト
		$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
	}

	// パスワード変更
	public function change_password(){
		if($this->request->isPost() || $this->request->isPut()){
			if(!empty($this->request->data)){
				$db_search = $this->User->find('first', array(
					'fields' => array(
						'User.id',
					),
					'conditions' => array(
						'User.id' => $this->Auth->user()['id'],
						'User.password' => AuthComponent::password($this->request->data['User']['old_password']),
					)
				));
				if(isset($db_search['User']['id']) == true){
					$submit_data = array(
						'User' => array(
							'id' => $this->Auth->user()['id'],
							'password' => $this->request->data['User']['new_password']
						)
					);
					if($this->User->save($submit_data)){
						$this->Session->setFlash('password_changed', null, null, 'password_changed');
						// indexにリダイレクト
						$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
						return;

					}else{
						$this->Session->setFlash('failded', null, null, 'change_password_status');
						$this->Session->setFlash('New password must contain 4 - 20 characters.', 'flash_alert_danger', null, 'change_password_message');
					}
				}else{
					$this->Session->setFlash('failded', null, null, 'change_password_status');
					$this->Session->setFlash('Your old password is not correct.', 'flash_alert_danger', null, 'change_password_message');
				}
			}
		}
		// indexにリダイレクト
		$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
	}
}
