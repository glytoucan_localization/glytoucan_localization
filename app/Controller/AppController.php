<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	// 使用するコンポーネントの設定
	public $components = array(  
    'Session',  
    'Cookie',  

    'Auth' => array(  
    	// ログイン処理用アクションの設定
    	'loginAction' => array(
    		'controller' => 'localizations',
    		'action' => 'index'
    	),

    	// ログアウト処理用アクションの設定
    	'logoutAction' => array(
    		'controller' => 'localizations',
    		'action' => 'index'
    	),

    	'authenticate' => array(
    		// Formを使った認証設定
    		'Form' => array(
    			// 認証に使用するモデルを指定
    			'userModel' => 'User',

    			// 認証に使用するフィールドを指定
    			'fields' => array(
    				'username' => 'name',
    				'password' => 'password'
    			)
    		),
    	),

      'flash' => array(  
        'element' => 'alert',  
        'key' => 'auth',  
        'params' => array(  
          'plugin' => 'BoostCake',  
          'class' => 'alert-error'  
        ),  
      ),  
    ),  
  );  
  
  // 使用するヘルパーの設定
  public $helpers = array(  
    'Session',  
    'Html' => array('className' => 'BoostCake.BoostCakeHtml'),  
    'Form' => array('className' => 'BoostCake.BoostCakeForm'),  
    'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),  
  );  
}
