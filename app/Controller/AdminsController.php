<?php
class AdminsController extends AppController{

	// 使用するモデルを指定
	public $uses = array(
		'Language',
		'ControllerName', 
		'ViewName', 
		'Place',
	);

	// 使用するコンポーネントを指定
	public $components = array(
	);

	// beforeFilterコールバック(各アクション実行前に実行)
	public function beforeFilter(){
	}

	// Adminユーザー用コマンド
	public function admin_operation(){
    if($this->Auth->loggedIn()){
    	if($this->Auth->user()['is_admin'] == 1){
				// データ入力があるか
				if($this->request->isPost() || $this->request->isPut()){
					if(!empty($this->request->data)){
		        $post_data = array();
		        if(isset($this->request->data['Article']) == true){
			        $post_data = $this->request->data['Article'];
		        }else if(isset($this->request->data['NavigationMenu']) == true){
			        $post_data = $this->request->data['NavigationMenu'];
		        }

						// Admin用データベース要素変更命令
						$submit_value = null;
		      	$operation_type = -1;

			      if(isset($this->request->data['submit_add_lang']) == true){
			      	$operation_type = 1;
				      if(isset($post_data['add_lang_name']) == true){
				      	$add_lang_name = $post_data['add_lang_name'];
				      	if($add_lang_name != ''){
				      		$submit_value = array(
				      			'Language' => array(
				      				'name' => $add_lang_name,
				      			),
				      		);
				      	}
				      }
			      }else if(isset($this->request->data['submit_add_cont_name']) == true){
			      	$operation_type = 2;
				      if(isset($post_data['add_cont_name']) == true){
				      	$add_cont_name = $post_data['add_cont_name'];
				      	if($add_cont_name != ''){
				      		$submit_value = array(
				      			'ControllerName' => array(
				      				'name' => $add_cont_name,
				      			),
				      		);
				      	}
				      }
			      }else if(isset($this->request->data['submit_add_view_name']) == true){
			      	$operation_type = 3;
				      if( (isset($post_data['add_cont_name_of_view']) == true) &&
				      		(isset($post_data['add_view_name']) == true) ){
				      	$add_cont_name_of_view = $post_data['add_cont_name_of_view'];
				      	$add_view_name = $post_data['add_view_name'];
				      	if($add_cont_name_of_view != '' && $add_view_name != ''){
				      		$foreign_cont_id = $this->ControllerName->find('first', array(
				      			'fields' => array(
				      				'ControllerName.id',
				      			),
				      			'conditions' => array(
				      				'ControllerName.name' => $add_cont_name_of_view,
				      			),
				      		));
				      		if(isset($foreign_cont_id['ControllerName']['id']) == true){
					      		$submit_value = array(
					      			'ViewName' => array(
					      				'cont_id' => $foreign_cont_id['ControllerName']['id'],
					      				'name' => $add_view_name,
					      			),
					      		);
				      		}
				      	}
				      }
			      }else if(isset($this->request->data['submit_add_place']) == true){
			      	$operation_type = 4;
				      if(isset($post_data['add_place_name']) == true){
				      	$add_place_name = $post_data['add_place_name'];
				      	if($add_place_name != ''){
				      		$submit_value = array(
				      			'Place' => array(
				      				'name' => $add_place_name,
				      			),
				      		);
				      	}
				      }
			      }else if(isset($this->request->data['submit_change_lang']) == true){
			      	$operation_type = 5;
			      	if( (isset($post_data['old_lang_name']) == true) &&
			      			(isset($post_data['new_lang_name']) == true) ){
				      	$old_lang_name = $post_data['old_lang_name'];
				      	$new_lang_name = $post_data['new_lang_name'];
				      	if($old_lang_name != '' && $new_lang_name != ''){
				      		$old_lang_data = $this->Language->find('first', array(
				      			'fields' => array(
				      				'Language.id',
				      			),
				      			'conditions' => array(
				      				'Language.name' => $old_lang_name,
				      			),
				      		));
				      		if(isset($old_lang_data['Language']['id']) == true){
				      			$submit_value = array(
				      				'Language' => array(
				      					'id' => $old_lang_data['Language']['id'],
				      					'name' => $new_lang_name,
				      				),
				      			);
				      		}
				      	}
			      	}
			      }else if(isset($this->request->data['submit_change_cont_name']) == true){
			      	$operation_type = 6;
				      if( (isset($post_data['old_cont_name']) == true) &&
				      		(isset($post_data['new_cont_name']) == true) ){
				      	$old_cont_name = $post_data['old_cont_name'];
				      	$new_cont_name = $post_data['new_cont_name'];
				      	if($old_cont_name != '' && $new_cont_name != ''){
				      		$old_cont_data = $this->ControllerName->find('first', array(
				      			'fields' => array(
				      				'ControllerName.id',
				      			),
				      			'conditions' => array(
				      				'ControllerName.name' => $old_cont_name,
				      			),
				      		));
				      		if(isset($old_cont_data['ControllerName']['id']) == true){
				      			$submit_value = array(
				      				'ControllerName' => array(
				      					'id' => $old_cont_data['ControllerName']['id'],
				      					'name' => $new_cont_name,
				      				),
				      			);
				      		}
				      	}
				      }
			      }else if(isset($this->request->data['submit_change_view_name']) == true){
			      	$operation_type = 7;
				      if( (isset($post_data['old_cont_name_of_view']) == true) &&
				      		(isset($post_data['old_view_name']) == true) &&
				      		(isset($post_data['new_cont_name_of_view']) == true) &&
				      		(isset($post_data['new_view_name']) == true) ){
				      	$old_cont_name_of_view = $post_data['old_cont_name_of_view'];
				      	$old_view_name = $post_data['old_view_name'];
				      	$new_cont_name_of_view = $post_data['new_cont_name_of_view'];
				      	$new_view_name = $post_data['new_view_name'];
				      	if($old_cont_name_of_view != '' && $old_view_name != '' &&
				      		 $new_cont_name_of_view != '' && $new_view_name != '' ){

				      		$old_foreign_cont_id = $this->ControllerName->find('first', array(
				      			'fields' => array(
				      				'ControllerName.id',
				      			),
				      			'conditions' => array(
				      				'ControllerName.name' => $old_cont_name_of_view,
				      			),
				      		));

				      		if(isset($old_foreign_cont_id['ControllerName']['id']) == true){
				      			$old_view_data = $this->ViewName->find('first', array(
				      				'fields' => array(
				      					'ViewName.id',
				      				),
				      				'conditions' => array(
				      					'ViewName.cont_id' => $old_foreign_cont_id['ControllerName']['id'],
				      					'ViewName.name' => $old_view_name,
				      				),
				      			));

					      		if(isset($old_view_data['ViewName']['id']) == true){
					      			$new_foreign_cont_id = $this->ControllerName->find('first', array(
					      				'fields' => array(
					      					'ControllerName.id',
					      				),
					      				'conditions' => array(
					      					'ControllerName.name' => $new_cont_name_of_view,
					      				),
					      			));

						      		if(isset($new_foreign_cont_id['ControllerName']['id']) == true){
							      		$submit_value = array(
							      			'ViewName' => array(
							      				'id' => $old_view_data['ViewName']['id'],
							      				'cont_id' => $new_foreign_cont_id['ControllerName']['id'],
							      				'name' => $new_view_name,
							      			),
							      		);
						      		}
					      		}
				      		}
				      	}
				      }
			      }else if(isset($this->request->data['submit_change_place']) == true){
			      	$operation_type = 8;
				      if( (isset($post_data['old_place_name']) == true) &&
				      		(isset($post_data['new_place_name']) == true) ){
				      	$old_place_name = $post_data['old_place_name'];
				      	$new_place_name = $post_data['new_place_name'];
				      	if($old_place_name != '' && $new_place_name != ''){
				      		$old_place_data = $this->Place->find('first', array(
				      			'fields' => array(
				      				'Place.id',
				      			),
				      			'conditions' => array(
				      				'Place.name' => $old_place_name,
				      			),
				      		));
				      		if(isset($old_place_data['Place']['id']) == true){
				      			$submit_value = array(
				      				'Place' => array(
				      					'id' => $old_place_data['Place']['id'],
				      					'name' => $new_place_name,
				      				),
				      			);
				      		}
				      	}
				      }
			      }

		      	$admin_operation_is_error = false;
		      	$admin_operation_error_message = "";

			      if($submit_value != null){
			      	try{
			      		switch($operation_type){
			      			case 1:
						      	if($this->Language->saveAll($submit_value)){
						      		// Success
						      	}
			      				break;
			      			case 2:
						      	if($this->ControllerName->saveAll($submit_value)){
						      		// Success
						      	}
						      	break;
						      case 3:
						      	if($this->ViewName->saveAll($submit_value)){
						      		// Success
						      	}
						      	break;
						      case 4:
						      	if($this->Place->saveAll($submit_value)){
						      		// Success
						      	}
						      	break;
						      case 5:
						      	if($this->Language->saveAll($submit_value)){
						      		// Success
						      	}
						      	break;
						      case 6:
						      	if($this->ControllerName->saveAll($submit_value)){
						      		// Success
						      	}
						      	break;
						      case 7:
						      	if($this->ViewName->saveAll($submit_value)){
						      		// Success
						      	}
						      	break;
						      case 8:
						      	if($this->Place->saveAll($submit_value)){
						      		// Success
						      	}
						      	break;
			      		}
			      	}catch(Exception $e){
			      		// 例外対処
				      	$admin_operation_is_error = true;
				      	switch($operation_type){
				      		case 1:
				      			$admin_operation_error_message = "Requested language is already registered.";
					      		break;
				      		case 2:
				      			$admin_operation_error_message = "Requested controller name is already registered.";
					      		break;
				      		case 3:
				      			$admin_operation_error_message = "Requested view name is already registered.";
					      		break;
				      		case 4:
				      			$admin_operation_error_message = "Requested place name is already registered.";
					      		break;
				      		case 5:
				      			$admin_operation_error_message = "Requested language is already registered.";
					      		break;
				      		case 6:
				      			$admin_operation_error_message = "Requested controller name is already registered.";
					      		break;
				      		case 7:
				      			$admin_operation_error_message = "Requested view name is already registered.";
					      		break;
				      		case 8:
				      			$admin_operation_error_message = "Requested place is already registered.";
					      		break;
				      	}
			      	}
			      }else{
			      	$admin_operation_is_error = true;
			      	switch($operation_type){
			      		case 1:
			      			$admin_operation_error_message = "You cannot submit blank form.";
				      		break;
			      		case 2:
			      			$admin_operation_error_message = "You cannot submit blank form.";
				      		break;
			      		case 3:
			      			$admin_operation_error_message = "Requested old controller name is not registered.";
				      		break;
			      		case 4:
			      			$admin_operation_error_message = "You cannot submit blank form.";
				      		break;
			      		case 5:
			      			$admin_operation_error_message = "Requested old language is not registered.";
				      		break;
			      		case 6:
			      			$admin_operation_error_message = "Requested old controller name is not registered.";
				      		break;
			      		case 7:
			      			$admin_operation_error_message = "Request parameters are not correct.";
				      		break;
			      		case 8:
			      			$admin_operation_error_message = "Requested old place is not registered.";
				      		break;
			      	}
			      }

			      if($admin_operation_is_error){
				      $this->Session->setFlash('error', null, null, 'admin_operation');
				      $this->Session->setFlash($admin_operation_error_message, null, null, 'admin_operation_message');
			      }else{
				      $this->Session->setFlash('success', null, null, 'admin_operation');
				      $this->Session->setFlash('Operation completed successfully.', null, null, 'admin_operation_message');
			      }
					}
				}
			}
		}
		$this->redirect(array('controller' => 'localizations', 'action' => 'index'));
		return;
	}
}
