<?php
class LocalizationsController extends AppController{

	// 使用するモデルを指定
	public $uses = array(
		'User',
		'Language',
		'ControllerName',
		'ViewName',
		'Place',
		'NavigationMenuItem',
		'NavigationMenu',
		'CommonItem',
		'Common',
		'Article',
		'ChangeLog',
		'ContentType',
	);

	// 使用するコンポーネントを指定
	public $components = array(
	);

	// 使用するレイアウトを指定
	public $layout = 'localization';

	// beforeFilterコールバック(各アクション実行前に実行)
	public function beforeFilter(){
		// ログイン無しでアクセスできるアクションを列挙する 引数なしで全アクションアクセス可能にする
		$this->Auth->allow('index', 'get_json');
		// 認証コンポーネントをViewで利用可能にしておく
		$this->set('auth', $this->Auth);
	}

	// beforeRenderコールバック(各ビューレンダリング前に実行)
	public function beforeRender(){
		// Admin権限の有無
		$is_admin = ($this->Auth->user()['is_admin']==1) ? true : false;
		$this->set('isAdmin', $is_admin);

		// フォーム用リスト
		$this->set('contentTypeList', $this->ContentType->find('list', array('order' => 'ContentType.id ASC')));
		$this->set('languageList', $this->Language->find('list', array('order' => 'Language.id ASC')));
		$this->set('controllerNameList', $this->ControllerName->find('list', array('order' => 'ControllerName.id ASC')));
		$this->set('viewNameList', $this->ViewName->find('list', array('order' => 'ViewName.id ASC')));
		$this->set('placeList', $this->Place->find('list', array('order' => 'Place.id ASC')));
		$this->set('navigationMenuItemList', $this->NavigationMenuItem->find('list', array('order' => 'NavigationMenuItem.id ASC')));
		$this->set('commonItemList', $this->CommonItem->find('list', array('order' => 'CommonItem.id ASC')));

		$langs = $this->Language->find('list', array('order' => 'Language.id ASC'));
		$cont_names = $this->ControllerName->find('list', array('order' => 'ControllerName.id ASC'));
		$view_names = $this->ViewName->find('all', array(
			'fields' => array(
				'ViewName.id',
				'ViewName.name',
				'ControllerName.id'
			),
			'order' => array(
				'ViewName.id ASC',
			),
		));
		$commons = $this->Common->find('all', array(
			'fields' => array(
				'Common.id',
				'Common.lang_id',
				'Common.modified_date',
			),
			'order' => array(
				'Common.id ASC',
			)
		));
		$nav_menus = $this->NavigationMenu->find('all', array(
			'fields' => array(
				'NavigationMenu.id',
				'NavigationMenu.lang_id',
				'NavigationMenu.modified_date',
			),
			'order' => array(
				'NavigationMenu.id ASC',
			),
		));
		$articles = $this->Article->find('all', array(
			'fields' => array(
				'Article.lang_id',
				'ViewName.cont_id',
				'Article.view_id',
				'Article.place_id',
				'Article.num',
				'Article.modified_date',
			),
			'order' => array(
				'Article.id ASC',
			),
		));

		// 国情報とコントローラ情報をセット
		$tree = array();
		foreach($langs as $lang_id => $lang){
			$lang_data = array();
			$lang_data['lang_id'] = $lang_id;
			$lang_data['lang_name'] = $lang;
			$lang_data['modified_date'] = '0000-00-00 00:00:00';
			$lang_data['controllers'] = array();

			// NavigationMenuはビューに依存しない部分のローカライズ用
			$nav_menu_data = array();
			$nav_menu_data['nav_name'] = 'Navigation Menu';
			$nav_menu_data['modified_date'] = '0000-00-00 00:00:00';
			$lang_data['navigation_menu'] = $nav_menu_data;

			// Commonは各ページで繰り返し使用されるパーツのローカライズ用
			$common_data = array();
			$common_data['common_name'] = 'Common';
			$common_data['modified_date'] = '0000-00-00 00:00:00';
			$lang_data['common'] = $common_data;

			foreach($cont_names as $cont_id => $cont_name){
				$cont_data = array();
				$cont_data['cont_id'] = $cont_id;
				$cont_data['cont_name'] = $cont_name;
				$cont_data['modified_date'] = '0000-00-00 00:00:00';
				$cont_data['views'] = array();
				$lang_data['controllers'][] = $cont_data;
			}
			$tree[] = $lang_data;
		}

		// ビュー情報（ページ情報をセット）
		for($i=0;$i<count($view_names);$i++){
			$cont_id = $view_names[$i]['ControllerName']['id'];
			$view_id = $view_names[$i]['ViewName']['id'];
			$view_name = $view_names[$i]['ViewName']['name'];

			for($j=0;$j<count($tree);$j++){
				for($k=0;$k<count($tree[$j]['controllers']);$k++){
					if($tree[$j]['controllers'][$k]['cont_id'] == $cont_id){
						$view_data = array();
						$view_data['view_id'] = $view_id;
						$view_data['view_name'] = $view_name;
						$view_data['modified_date'] = '0000-00-00 00:00:00';
						$tree[$j]['controllers'][$k]['views'][] = $view_data;
					}
				}
			}
		}

		// Articleを走査し、ビュー（ページ）の更新履歴情報をセットする
		foreach($articles as $article){
			for($i=0;$i<count($tree);$i++){
				if($article['Article']['lang_id'] == $tree[$i]['lang_id']){
					for($j=0;$j<count($tree[$i]['controllers']);$j++){
						if($article['ViewName']['cont_id'] == $tree[$i]['controllers'][$j]['cont_id']){
							for($k=0;$k<count($tree[$i]['controllers'][$j]['views']);$k++){
								if($article['Article']['view_id'] == $tree[$i]['controllers'][$j]['views'][$k]['view_id']){
									if( strtotime($article['Article']['modified_date']) > strtotime($tree[$i]['controllers'][$j]['views'][$k]['modified_date']) ){
										$tree[$i]['controllers'][$j]['views'][$k]['modified_date'] = $article['Article']['modified_date'];
									}
								}
							}
						}
					}
				}
			}
		}

		// Navigation Menuの更新履歴情報をセットする
		for($i=0;$i<count($tree);$i++){
			foreach($nav_menus as $nav_menu_id => $nav_menu){
				if($tree[$i]['lang_id'] == $nav_menu['NavigationMenu']['lang_id']){
					if( strtotime($tree[$i]['navigation_menu']['modified_date']) < strtotime($nav_menu['NavigationMenu']['modified_date']) ){
						$tree[$i]['navigation_menu']['modified_date'] = $nav_menu['NavigationMenu']['modified_date'];
					}
				}
			}
		}

		// Commonの更新履歴情報をセットする
		for($i=0;$i<count($tree);$i++){
			foreach($commons as $common_id => $common_data){
				if($tree[$i]['lang_id'] == $common_data['Common']['lang_id']){
					if( strtotime($tree[$i]['common']['modified_date']) < strtotime($common_data['Common']['modified_date']) ){
						$tree[$i]['common']['modified_date'] = $common_data['Common']['modified_date'];
					}
				}
			}
		}

		// ビュー（ページ）の更新履歴情報を元に、コントローラと国ごとの更新履歴情報をセットする
		for($i=0;$i<count($tree);$i++){
			$lang_newest_date = '0000-00-00 00:00:00';
			for($j=0;$j<count($tree[$i]['controllers']);$j++){
				$cont_newest_date = '0000-00-00 00:00:00';
				for($k=0;$k<count($tree[$i]['controllers'][$j]['views']);$k++){
					if( strtotime($cont_newest_date) < strtotime($tree[$i]['controllers'][$j]['views'][$k]['modified_date']) ){
						$cont_newest_date = $tree[$i]['controllers'][$j]['views'][$k]['modified_date'];
					}
				}
				$tree[$i]['controllers'][$j]['modified_date'] = $cont_newest_date;
				if( strtotime($lang_newest_date) < strtotime($cont_newest_date) ){
					$lang_newest_date = $cont_newest_date;
				}
			}
			if( strtotime($lang_newest_date) < strtotime($tree[$i]['navigation_menu']['modified_date']) ){
				$lang_newest_date = $tree[$i]['navigation_menu']['modified_date'];
			}
			if( strtotime($lang_newest_date) < strtotime($tree[$i]['common']['modified_date']) ){
				$lang_newest_date = $tree[$i]['common']['modified_date'];
			}
			$tree[$i]['modified_date'] = $lang_newest_date;
		}
		$this->set('tree', $tree);
	}
	

	public function index(){
		// タイトル
		$this->set('title_for_layout', 'GlyTouCan Localization Form');

		// ChangeLogセット
		$this->set('change_logs', $this->ChangeLog->get_change_logs(100));
		$this->render('index');
	}

	private function reset_glytoucan_docs(){
		$result = file_get_contents("http://yamazaki.glytoucan.org/languages/rmtmp");
	}

	public function article(){
		// タイトル
		$this->set('title_for_layout', 'GlyTouCan Localization Form');

		// ContentTypeリスト
		$contentTypeList = $this->ContentType->find('list', array('order' => 'ContentType.id ASC'));

		if($this->Auth->loggedIn()){

			// データ入力があるか
			if($this->request->isPost() || $this->request->isPut()){
				if(!empty($this->request->data)){

					$param = array();
					$post_data = array();
					if(isset($this->request->data['Article']) == true){
						$post_data = $this->request->data['Article'];
					}else if(isset($this->request->data['NavigationMenu']) == true){
						$post_data = $this->request->data['NavigationMenu'];
					}else if(isset($this->request->data['Common']) == true){
						$post_data = $this->request->data['Common'];
					}

					$param['selected_lang_id'] = $post_data['selected_lang_id'];
					$param['selected_cont_name_id'] = $post_data['selected_cont_name_id'];
					$param['selected_view_name_id'] = $post_data['selected_view_name_id'];

					// データ保存要求
					if(isset($post_data['data_submit']) == true){
						$keys = array_keys($post_data);
						// 一般更新
						$inputed_articles_keys = array_merge(preg_grep('/^art_/', $keys));
						$inputed_articles_disabled_keys = array_merge(preg_grep('/^disable-art_/', $keys));
						$inputed_articles = array();
						/*
						foreach($inputed_articles_keys as $inputed_articles_key){
							// 空白入力は捨てる
							if($post_data[$inputed_articles_key] != ''){
								$inputed_articles[$inputed_articles_key] = $post_data[$inputed_articles_key];
							}
						}
						*/
						for($i=0;$i<count($inputed_articles_keys);$i++){
							// 空白入力もまとめて取得する
							$tmp = array();
							$tmp['text'] = $post_data[ $inputed_articles_keys[$i] ];

							// disabled情報を付加する
							if(in_array("disable-".$inputed_articles_keys[$i], $inputed_articles_disabled_keys)){
								$tmp['disabled'] = $post_data[ "disable-".$inputed_articles_keys[$i] ];
							}else{
								$tmp['disabled'] = 0;
							}
							$inputed_articles[ $inputed_articles_keys[$i] ] = $tmp;
						}

						// Admin要素追加
						$added_articles_keys = array_merge(preg_grep('/^new-art_/', $keys));
						$added_articles = array();
						foreach($added_articles_keys as $added_articles_key){
							// 空白入力は捨てる
							if($post_data[$added_articles_key] != ''){
								$added_articles[$added_articles_key] = $post_data[$added_articles_key];
							}
						}

						$submit_results = array();

						// 一般更新時のデータベース更新実行
						foreach($inputed_articles as $key => $value){
							$place_and_num = explode('_', $key);
							$inputed_place_id = $place_and_num[1];
							$inputed_num = $place_and_num[2];

							// 該当するarticleの最新データを取得する
							$old_db_data = $this->Article->find('first', array(
								'fields' => array(
									'Article.created_date',
									'Article.article',
									'Article.disabled',
								),
								'conditions' => array(
									'Article.lang_id' => $param['selected_lang_id'],
									'Article.view_id' => $param['selected_view_name_id'],
									'Article.place_id' => $inputed_place_id,
									'Article.num' => $inputed_num,
								),
								'order' => array(
									'Article.modified_date DESC',
									'Article.id ASC',
								)
							));

							$submit_data = null;
							if(isset($old_db_data['Article']) == true){
								// データベース上にデータが存在する場合

								// 入力されたデータがデータベースの値と完全一致する場合には、保存を行わない
								if(($old_db_data['Article']['article'] != $value['text']) || ($old_db_data['Article']['disabled'] != $value['disabled'])){
									$submit_data = array(
										'Article' => array(
											'user_id' => $this->Auth->user()['id'],
											'lang_id' => $param['selected_lang_id'],
											'view_id' => $param['selected_view_name_id'],
											'place_id' => $inputed_place_id,
											'num' => $inputed_num,
											'created_date' => $old_db_data['Article']['created_date'],
											'article' => $value['text'],
											'disabled' => $value['disabled'],
										),
									);
								}
							}else{
								// データベース上にデータが存在しない場合
								// 入力が空白以外、あるいはdisabledが1ならば
								if(($value['text'] != "") || ($value['disabled'] != 0)){
									$submit_data = array(
										'Article' => array(
											'user_id' => $this->Auth->user()['id'],
											'lang_id' => $param['selected_lang_id'],
											'view_id' => $param['selected_view_name_id'],
											'place_id' => $inputed_place_id,
											'num' => $inputed_num,
											'created_date' => null,
											'article' => $value['text'],
											'disabled' => $value['disabled'],
										),
									);
								}
							}

							// データをデータベースに保存する
							if($submit_data != null){
								if($this->Article->saveAll($submit_data)){
									$submit_info = array(
										'lang_id' => $param['selected_lang_id'],
										'cont_name_id' => $param['selected_cont_name_id'],
										'view_name_id' => $param['selected_view_name_id'],
										'place_id' => $inputed_place_id,
										'num' => $inputed_num,
									);
									$submit_results[] = $submit_info;

									// ChangeLog追加
									$log_data = array(
										'ChangeLog' => array(
											'date' => null,
											'user_id' => $this->Auth->user()['id'],
											'content_type_id' => array_search('article', $contentTypeList),
											'target_id' => $this->Article->getLastInsertID(),
										),
									);
									$this->ChangeLog->saveAll($log_data);
								}
							}
						}

						// Admin要素追加時のデータベース更新実行
						foreach($added_articles as $key => $value){
							$place_and_num = explode('_', $key);
							$added_place_id = $place_and_num[1];
							$added_num = $place_and_num[2];

							$submit_data = array(
								'Article' => array(
									'user_id' => $this->Auth->user()['id'],
									'lang_id' => $param['selected_lang_id'],
									'view_id' => $param['selected_view_name_id'],
									'place_id' => $added_place_id,
									'num' => $added_num,
									'created_date' => null,
									'article' => $value,
								),
							);

							// データをデータベースに保存する
							if($submit_data != null){
								if($this->Article->saveAll($submit_data)){
									$submit_info = array(
										'lang_id' => $param['selected_lang_id'],
										'cont_name_id' => $param['selected_cont_name_id'],
										'view_name_id' => $param['selected_view_name_id'],
										'place_id' => $added_place_id,
										'num' => $added_num,
									);
									$submit_results[] = $submit_info;

									// ChangeLog追加
									$log_data = array(
										'ChangeLog' => array(
											'date' => null,
											'user_id' => $this->Auth->user()['id'],
											'content_type_id' => array_search('article', $contentTypeList),
											'target_id' => $this->Article->getLastInsertID(),
										),
									);
									$this->ChangeLog->saveAll($log_data);
								}
							}
						}
						$this->set('submit_results', $submit_results);

						$this->reset_glytoucan_docs();
					}

					// データ読み込み

					// 該当するViewについての保存されているArticleは（当然）すべて取得する
					// ただし、場合によってはデータが保存されていないこともある。
					// その場合、languagesテーブルのid順に走査し、他の言語でそのPlaceに既に入力されている値を取得してきてセットする。
					// 全くデータがない場合には、諦めて空欄表示する(placeholderで"Input here..."とか表示)
					// 全てのあり得るPlaceごとに、現在登録されているnumの数を算出し、各Placeごとにnumが最大数出るように調整する
					// 追加する場合にはAdminでView最下のテキストボックスを利用する

					// 言語データの取得開始
					$all_place_data = array();
					$all_place_data['selected_lang_id'] = $param['selected_lang_id'];
					$all_place_data['selected_cont_name_id'] = $param['selected_cont_name_id'];
					$all_place_data['selected_view_name_id'] = $param['selected_view_name_id'];
					$all_place_data['places'] = $this->Article->get_place_data($all_place_data['selected_lang_id'], $all_place_data['selected_view_name_id']);
					$this->set('view_data', $all_place_data);
					$this->render('article');
					return;
				}
			}
		}
		$this->redirect(array('action' => 'index'));
		return;
	}

	public function navigation_menu(){
		// タイトル
		$this->set('title_for_layout', 'GlyTouCan Localization Form');

		// ContentTypeリスト
		$contentTypeList = $this->ContentType->find('list', array('order' => 'ContentType.id ASC'));

		if($this->Auth->loggedIn()){
			// データ入力があるか
			if($this->request->isPost() || $this->request->isPut()){
				if(!empty($this->request->data)){
					$param = array();
					$post_data = array();
					if(isset($this->request->data['Article']) == true){
						$post_data = $this->request->data['Article'];
					}else if(isset($this->request->data['NavigationMenu']) == true){
						$post_data = $this->request->data['NavigationMenu'];
					}else if(isset($this->request->data['Common']) == true){
						$post_data = $this->request->data['Common'];
					}
					$param['selected_lang_id'] = $post_data['selected_lang_id'];

					// データ保存要求
					if(isset($post_data['data_submit']) == true){
						$keys = array_keys($post_data);
						// 一般更新
						$inputed_menu_items_keys = array_merge(preg_grep('/^nav_/', $keys));
						$inputed_menu_items_disabled_keys = array_merge(preg_grep('/^disable-nav_/', $keys));
						$inputed_menu_items = array();
						/*
						foreach($inputed_menu_items_keys as $inputed_menu_items_key){
							// 空白入力は捨てる
							if($post_data[$inputed_menu_items_key] != ''){
								$inputed_menu_items[$inputed_menu_items_key] = $post_data[$inputed_menu_items_key];
							}
						}
						*/
						for($i=0;$i<count($inputed_menu_items_keys);$i++){
							// 空白入力もまとめて取得する
							$tmp = array();
							$tmp['text'] = $post_data[ $inputed_menu_items_keys[$i] ];

							// disabled情報を付加する
							if(in_array("disable-".$inputed_menu_items_keys[$i], $inputed_menu_items_disabled_keys)){
								$tmp['disabled'] = $post_data[ "disable-".$inputed_menu_items_keys[$i] ];
							}else{
								$tmp['disabled'] = 0;
							}
							$inputed_menu_items[ $inputed_menu_items_keys[$i] ] = $tmp;
						}

						// Admin要素追加
						$added_menu_items_keys = array_merge(preg_grep('/^new-nav/', $keys));
						$added_menu_items = array();
						foreach($added_menu_items_keys as $added_menu_items_key){
							// 空白入力は捨てる
							if($post_data[$added_menu_items_key] != ''){
								$added_menu_items[$added_menu_items_key] = $post_data[$added_menu_items_key];
							}
						}

						$submit_results = array();

						// 一般更新時のデータベース更新実行
						foreach($inputed_menu_items as $key => $value){
							$item_id_tmp = explode('_', $key);
							$inputed_item_id = $item_id_tmp[1];

							// 該当するnavigation_menuの最新データを取得する
							$old_db_data = $this->NavigationMenu->find('first', array(
								'fields' => array(
									'NavigationMenu.created_date',
									'NavigationMenu.data',
									'NavigationMenu.disabled',
								),
								'conditions' => array(
									'NavigationMenu.lang_id' => $param['selected_lang_id'],
									'NavigationMenu.menu_item_id' => $inputed_item_id,
								),
								'order' => array(
									'NavigationMenu.modified_date DESC',
									'NavigationMenu.id ASC',
								)
							));

							$submit_data = null;
							if(isset($old_db_data['NavigationMenu']) == true){
								// データベース上にデータが存在する場合

								// 入力されたデータがデータベースの値と完全一致する場合には、保存を行わない
								if(($old_db_data['NavigationMenu']['data'] != $value['text']) || ($old_db_data['NavigationMenu']['disabled'] != $value['disabled'])){
									$submit_data = array(
										'NavigationMenu' => array(
											'user_id' => $this->Auth->user()['id'],
											'lang_id' => $param['selected_lang_id'],
											'menu_item_id' => $inputed_item_id,
											'created_date' => $old_db_data['NavigationMenu']['created_date'],
											'data' => $value['text'],
											'disabled' => $value['disabled']
										),
									);
								}
							}else{
								// データベース上にデータが存在しない場合
								// 入力が空白以外、あるいはdisabledが1ならば
								if(($value['text'] != "") || ($value['disabled'] != 0)){
									$submit_data = array(
										'NavigationMenu' => array(
											'user_id' => $this->Auth->user()['id'],
											'lang_id' => $param['selected_lang_id'],
											'menu_item_id' => $inputed_item_id,
											'created_date' => null,
											'data' => $value['text'],
											'disabled' => $value['disabled']
										),
									);
								}
							}

							// データをデータベースに保存する
							if($submit_data != null){
								if($this->NavigationMenu->saveAll($submit_data)){
									$submit_info = array(
										'lang_id' => $param['selected_lang_id'],
										'menu_item_id' => $inputed_item_id,
									);
									$submit_results[] = $submit_info;

									// ChangeLog追加
									$log_data = array(
										'ChangeLog' => array(
											'date' => null,
											'user_id' => $this->Auth->user()['id'],
											'content_type_id' => array_search('navigation_menu', $contentTypeList),
											'target_id' => $this->NavigationMenu->getLastInsertID(),
										),
									);
									$this->ChangeLog->saveAll($log_data);
								}
							}
						}

						// Admin要素追加時のデータベース更新実行
						foreach($added_menu_items as $key => $value){
							// 既に登録されているか確認
							$db_data = $this->NavigationMenuItem->find('first', array(
								'conditions' => array(
									'NavigationMenuItem.name' => $value
								)
							));

							if(isset($db_data['NavigationMenuItem']) == false){
								$submit_data = array(
									'NavigationMenuItem' => array(
										'name' => $value,
									),
								);

								// データをデータベースに保存する
								// ChangeLogへの追加は行わない
								if($submit_data != null){
									if($this->NavigationMenuItem->saveAll($submit_data)){
										$submit_info = array(
											'added_menu_item' => $value,
										);
										$submit_results[] = $submit_info;
									}
								}
							}else{
								$submit_info = array(
									'error' => $value,
								);
								$submit_errors[] = $submit_info;
							}
						}
						$this->set('submit_results', $submit_results);
						if(isset($submit_errors) == true){
							$this->set('submit_errors', $submit_errors);
						}

						$this->reset_glytoucan_docs();
					}

					// データ読み込み

					// ナビゲーションデータの取得開始
					$all_nav_data = array();
					$all_nav_data['selected_lang_id'] = $param['selected_lang_id'];
					$all_nav_data['items'] = $this->NavigationMenu->get_navigation_menu_data($all_nav_data['selected_lang_id']);

					$this->set('view_data', $all_nav_data);

					$this->render('navigation_menu');
					return;
				}
			}
		}
		$this->redirect(array('action' => 'index'));
		return;
	}

	public function common(){
		// タイトル
		$this->set('title_for_layout', 'GlyTouCan Localization Form');

		// ContentTypeリスト
		$contentTypeList = $this->ContentType->find('list', array('order' => 'ContentType.id ASC'));

		if($this->Auth->loggedIn()){
			// データ入力があるか
			if($this->request->isPost() || $this->request->isPut()){
				if(!empty($this->request->data)){
					$param = array();
					$post_data = array();
					if(isset($this->request->data['Article']) == true){
						$post_data = $this->request->data['Article'];
					}else if(isset($this->request->data['NavigationMenu']) == true){
						$post_data = $this->request->data['NavigationMenu'];
					}else if(isset($this->request->data['Common']) == true){
						$post_data = $this->request->data['Common'];
					}
					$param['selected_lang_id'] = $post_data['selected_lang_id'];

					// データ保存要求
					if(isset($post_data['data_submit']) == true){
						$keys = array_keys($post_data);
						// 一般更新
						$inputed_common_items_keys = array_merge(preg_grep('/^common_/', $keys));
						$inputed_common_items_disabled_keys = array_merge(preg_grep('/^disable-common_/', $keys));
						$inputed_common_items = array();
						/*
						foreach($inputed_common_items_keys as $inputed_common_items_key){
							// 空白入力は捨てる
							if($post_data[$inputed_common_items_key] != ''){
								$inputed_common_items[$inputed_common_items_key] = $post_data[$inputed_common_items_key];
							}
						}
						*/
						for($i=0;$i<count($inputed_common_items_keys);$i++){
							// 空白入力もまとめて取得する
							$tmp = array();
							$tmp['text'] = $post_data[ $inputed_common_items_keys[$i] ];

							// disabled情報を付加する
							if(in_array("disable-".$inputed_common_items_keys[$i], $inputed_common_items_disabled_keys)){
								$tmp['disabled'] = $post_data[ "disable-".$inputed_common_items_keys[$i] ];
							}else{
								$tmp['disabled'] = 0;
							}
							$inputed_common_items[ $inputed_common_items_keys[$i] ] = $tmp;
						}

						// Admin要素追加
						$added_common_items_keys = array_merge(preg_grep('/^new-common/', $keys));
						$added_common_items = array();
						foreach($added_common_items_keys as $added_common_items_key){
							// 空白入力は捨てる
							if($post_data[$added_common_items_key] != ''){
								$added_common_items[$added_common_items_key] = $post_data[$added_common_items_key];
							}
						}

						$submit_results = array();
						$submit_errors = array();

						// 一般更新時のデータベース更新実行
						foreach($inputed_common_items as $key => $value){
							$item_id_tmp = explode('_', $key);
							$inputed_item_id = $item_id_tmp[1];

							// 該当するcommonの最新データを取得する
							$old_db_data = $this->Common->find('first', array(
								'fields' => array(
									'Common.created_date',
									'Common.data',
									'Common.disabled',
								),
								'conditions' => array(
									'Common.lang_id' => $param['selected_lang_id'],
									'Common.item_id' => $inputed_item_id,
								),
								'order' => array(
									'Common.modified_date DESC',
									'Common.id ASC',
								)
							));

							$submit_data = null;
							if(isset($old_db_data['Common']) == true){
								// データベース上にデータが存在する場合

								// 入力されたデータがデータベースの値と完全一致する場合には、保存を行わない
								if(($old_db_data['Common']['data'] != $value['text']) || ($old_db_data['Common']['disabled'] != $value['disabled'])){
									$submit_data = array(
										'Common' => array(
											'user_id' => $this->Auth->user()['id'],
											'lang_id' => $param['selected_lang_id'],
											'item_id' => $inputed_item_id,
											'created_date' => $old_db_data['Common']['created_date'],
											'data' => $value['text'],
											'disabled' => $value['disabled']
										),
									);
								}
							}else{
								// データベース上にデータが存在しない場合
								// 入力が空白以外、あるいはdisabledが1ならば
								if(($value['text'] != "") || ($value['disabled'] != 0)){
									$submit_data = array(
										'Common' => array(
											'user_id' => $this->Auth->user()['id'],
											'lang_id' => $param['selected_lang_id'],
											'item_id' => $inputed_item_id,
											'created_date' => null,
											'data' => $value['text'],
											'disabled' => $value['disabled']
										),
									);
								}
							}

							// データをデータベースに保存する
							if($submit_data != null){
								if($this->Common->saveAll($submit_data)){
									$submit_info = array(
										'lang_id' => $param['selected_lang_id'],
										'item_id' => $inputed_item_id,
									);
									$submit_results[] = $submit_info;

									// ChangeLog追加
									$log_data = array(
										'ChangeLog' => array(
											'date' => null,
											'user_id' => $this->Auth->user()['id'],
											'content_type_id' => array_search('common', $contentTypeList),
											'target_id' => $this->Common->getLastInsertID(),
										),
									);
									$this->ChangeLog->saveAll($log_data);
								}
							}
						}

						// Admin要素追加時のデータベース更新実行
						foreach($added_common_items as $key => $value){
							// 既に登録されているか確認
							$db_data = $this->CommonItem->find('first', array(
								'conditions' => array(
									'CommonItem.name' => $value
								)
							));

							if(isset($db_data['CommonItem']) == false){
								$submit_data = array(
									'CommonItem' => array(
										'name' => $value,
									),
								);

								// データをデータベースに保存する
								// ChangeLogへの追加は行わない
								if($submit_data != null){
									if($this->CommonItem->saveAll($submit_data)){
										$submit_info = array(
											'added_common_item' => $value,
										);
										$submit_results[] = $submit_info;
									}
								}
							}else{
								$submit_info = array(
									'error' => $value,
								);
								$submit_errors[] = $submit_info;
							}
						}
						$this->set('submit_results', $submit_results);
						if(isset($submit_errors) == true){
							$this->set('submit_errors', $submit_errors);
						}

						$this->reset_glytoucan_docs();
					}

					// データ読み込み

					// ナビゲーションデータの取得開始
					$all_common_data = array();
					$all_common_data['selected_lang_id'] = $param['selected_lang_id'];
					$all_common_data['items'] = $this->Common->get_common_data($all_common_data['selected_lang_id']);

					$this->set('view_data', $all_common_data);

					$this->render('common');
					return;
				}
			}
		}
		$this->redirect(array('action' => 'index'));
		return;
	}

	// 以下のコードで取得できる
	//	$url = "http://localhost:8888/glytoucan_localization/localizations/get_json/1.json";
	//	$json = file_get_contents($url, true);
	//	$arr = json_decode($json, true);
	//	print_r($arr);
	
	// 英語
	// $ret = file_get_contents("http://localhost:8888/glytoucan_localization/localizations/get_json/1.json", true);
	// 日本語
	// $ret = file_get_contents("http://localhost:8888/glytoucan_localization/localizations/get_json/2.json", true);
	// 中国語（簡体）
	// $ret = file_get_contents("http://localhost:8888/glytoucan_localization/localizations/get_json/3.json", true);
	// 中国語
	// $ret = file_get_contents("http://localhost:8888/glytoucan_localization/localizations/get_json/4.json", true);

	public function get_json($lang_id=1){
		// JSONのみを返すためViewのレンダーを無効化
		$this->autoRender = false;

		$status = -1;
		$result = array();
		$error = array();

		$lang_data = $this->Language->find('first', array(
			'fields' => array(
				'Language.id',
			),
			'conditions' => array(
				'Language.id' => $lang_id,
			),
		));
		if(isset($lang_data['Language']['id']) == true){
			$status = true;
		}else{
			$status = false;
			$error['message'] = "Not found";
		}

		if($status == true){
			$result = array(
				'article' => array(),
				'navigation_menu' => array(),
				'common' => array()
			);
			$cont_view_names = $this->Article->ViewName->find('all', array(
				'fields' => array(
					'ViewName.id',
					'ViewName.name',
					'ControllerName.id',
					'ControllerName.name'
				),
				'order' => array(
					'ViewName.id ASC'
				)
			));

			foreach($cont_view_names as $cont_view_name){
				$cont_name = $cont_view_name['ControllerName']['name'];
				$view_id = $cont_view_name['ViewName']['id'];
				$view_name = $cont_view_name['ViewName']['name'];
				if(isset($result['article'][$cont_name]) == false){
					$result['article'][$cont_name] = array();
				}
				$places = $this->Article->get_place_data($lang_id, $view_id);
				$result['article'][$cont_name][$view_name] = array();
				for($i=0;$i<count($places);$i++){
					// 空 あるいは disabled要素しかないPlaceは出力しない
					$article_flag = false;
					for($j=1;$j<=$places[$i]['max_num'];$j++){
						if($places[$i]['articles'][$j]['disabled'] != 1){
							if(($places[$i]['articles'][$j]['article'] != "") || ($places[$i]['articles'][$j]['ref_article'] != "")){
								$article_flag = true;
								$j = $places[$i]['max_num'];
							}
						}
					}

					if($article_flag == true){
						$result['article'][$cont_name][$view_name][ $places[$i]['place_name'] ] = array();
						for($j=1;$j<=$places[$i]['max_num'];$j++){
							if($places[$i]['articles'][$j]['disabled'] != 1){
								if($places[$i]['articles'][$j]['article'] != ""){
									$result['article'][$cont_name][$view_name][ $places[$i]['place_name'] ][ $places[$i]['articles'][$j]['num']-1 ] = $places[$i]['articles'][$j]['article'];
								}else if($places[$i]['articles'][$j]['ref_article'] != ""){
									$result['article'][$cont_name][$view_name][ $places[$i]['place_name'] ][ $places[$i]['articles'][$j]['num']-1 ] = $places[$i]['articles'][$j]['ref_article'];
								}
							}
						}
					}
				}
			}

			$nav_menus = $this->NavigationMenu->get_navigation_menu_data($lang_id);
			foreach($nav_menus as $nav_menu){
				if($nav_menu['disabled'] != 1){
					if($nav_menu['data'] != ""){
						$result['navigation_menu'][ $nav_menu['menu_item_name'] ] = $nav_menu['data'];
					}else if($nav_menu['ref_data'] != ""){
						$result['navigation_menu'][ $nav_menu['menu_item_name'] ] = $nav_menu['ref_data'];
					}
				}
			}

			$commons = $this->Common->get_common_data($lang_id);
			foreach($commons as $common){
				if($common['disabled'] != 1){
					if($common['data'] != ""){
						$result['common'][ $common['item_name'] ] = $common['data'];
					}else if($common['ref_data'] != ""){
						$result['common'][ $common['item_name'] ] = $common['ref_data'];
					}
				}
			}
		}
		// UTF-8で返却
		return json_encode(compact('status', 'result', 'error'), JSON_UNESCAPED_UNICODE);
	}

	public function browse_data(){
		// Admin権限の有無
		$is_admin = ($this->Auth->user()['is_admin']==1) ? true : false;
		if($is_admin){
			$this->autoRender = false;
			$langs = $this->Language->find('all', array(
				'fields' => array(
					'Language.id',
					'Language.name'
				),
				'order' => array(
					'Language.id ASC'
				),
			));
			for($i=0;$i<count($langs);$i++){
				$id = $langs[$i]['Language']['id'];
				$name = $langs[$i]['Language']['name'];
				$json = $this->get_json($id);
				echo "<<< ", $name, " >>>", PHP_EOL;
				print_r(json_decode($json, true));
				echo PHP_EOL;
			}
			return;
		}
		
		$this->redirect('index');
	}
}
