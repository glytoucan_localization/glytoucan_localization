<?php
class Common extends AppModel{
	public $name = 'common';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'commons';

	// アソシエーションの設定
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'Language' => array(
			'className' => 'Language',
			'foreignKey' => 'lang_id',
		),
		'CommonItem' => array(
			'className' => 'CommonItem',
			'foreignKey' => 'item_id',
		),
	);

	public $validate = array();

	public function get_common_data($lang_id){
		$ret = array();

		// メニューの一覧を取得し、デフォルト値を設定
		$common_item_data = $this->CommonItem->find('list', array('order' => 'CommonItem.id ASC'));
		foreach($common_item_data as $key => $value){
			$common_item = array();
			$common_item['item_id'] = $key;
			$common_item['item_name'] = $value;
			$common_item['created_date'] = '0000-00-00 00:00:00';
			$common_item['modified_date'] = '0000-00-00 00:00:00';
			$common_item['data'] = "";
			$common_item['ref_data'] = "";
			$common_item['disabled'] = -1;
			$ret[] = $common_item;
		}

		// 既にローカライズされている値を取得する
		$target_common_data = $this->find('all', array(
			'fields' => array(
				'Common.item_id',
				'Common.created_date',
				'Common.modified_date',
				'Common.data',
			),
			'conditions' => array(
				'Common.lang_id' => $lang_id,
			),
			'order' => array(
				'Common.modified_date DESC',
			),
		));
		foreach($target_common_data as $_target_common_data){
			for($i=0;$i<count($ret);$i++){
				if( ($ret[$i]['modified_date'] == '0000-00-00 00:00:00') &&
						($ret[$i]['item_id'] == $_target_common_data['Common']['item_id'])) {
					$ret[$i]['created_date'] = $_target_common_data['Common']['created_date'];
					$ret[$i]['modified_date'] = $_target_common_data['Common']['modified_date'];
					$ret[$i]['data'] = $_target_common_data['Common']['data'];
				}
			}
		}

/*
		// ローカライズされた値を取得できなかったアイテムを抽出
		$lacking_items = array();
		for($i=0;$i<count($ret);$i++){
			if(($ret[$i]['modified_date'] == '0000-00-00 00:00:00')){
				$lacking_item = array(
					'common_index' => $i,
					'item_id' => $ret[$i]['item_id'],
				);
				$lacking_items[] = $lacking_item;
			}
		}

		// ローカライズされていない部分に対し、他の言語から値を取得する
		$other_common_data = $this->find('all', array(
			'fields' => array(
				'Common.item_id',
				'Common.created_date',
				'Common.modified_date',
				'Common.data',
			),
			'order' => array(
				'Common.lang_id ASC',			// 国順にまず見る
				'Common.modified_date DESC',
			),
		));
		for($i=0;$i<count($lacking_items);$i++){
			foreach($other_common_data as $_other_common_data){
				if($_other_common_data['Common']['item_id'] == $lacking_items[$i]['item_id']){
					$ret[ $lacking_items[$i]['common_index'] ]['data'] = $_other_common_data['Common']['data'];
					break;
				}
			}
		}
*/
		// 英語のリファレンスデータを取得する
		$ref_common_data = $this->find('all', array(
			'fields' => array(
				'Common.item_id',
				'Common.created_date',
				'Common.modified_date',
				'Common.data',
				'Common.disabled',
			),
			'order' => array(
				'Common.lang_id ASC',			// 国順にまず見る
				'Common.modified_date DESC',
			),
		));
		for($i=0;$i<count($ret);$i++){
			foreach($ref_common_data as $common_data){
				if($ret[$i]['item_id'] == $common_data['Common']['item_id']){
					$ret[$i]['disabled'] = $common_data['Common']['disabled'];
					$ret[$i]['ref_data'] = $common_data['Common']['data'];
					break;
				}
			}
		}
		return $ret;

	}
}
