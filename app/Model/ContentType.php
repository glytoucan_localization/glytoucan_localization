<?php
class ContentType extends AppModel{
	public $name = 'content_type';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'content_types';

	// リスト表示するフィールドを指定
	public $displayField = 'name';

	public $validate = array();

}
