<?php
class NavigationMenu extends AppModel{
	public $name = 'navigation_menu';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'navigation_menus';

	// アソシエーションの設定
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'Language' => array(
			'className' => 'Language',
			'foreignKey' => 'lang_id',
		),
		'NavigationMenuItem' => array(
			'className' => 'NavigationMenuItem',
			'foreignKey' => 'menu_item_id',
		),
	);

	public $validate = array();

	public function get_navigation_menu_data($lang_id){
		$ret = array();

		// メニューの一覧を取得し、デフォルト値を設定
		$navigation_menu_item_data = $this->NavigationMenuItem->find('list', array('order' => 'NavigationMenuItem.id ASC'));
		foreach($navigation_menu_item_data as $key => $value){
			$nav_item = array();
			$nav_item['menu_item_id'] = $key;
			$nav_item['menu_item_name'] = $value;
			$nav_item['created_date'] = '0000-00-00 00:00:00';
			$nav_item['modified_date'] = '0000-00-00 00:00:00';
			$nav_item['data'] = "";
			$nav_item['ref_data'] = "";
			$nav_item['disabled'] = -1;
			$ret[] = $nav_item;
		}

		// 既にローカライズされている値を取得する
		$target_nav_menu = $this->find('all', array(
			'fields' => array(
				'NavigationMenu.menu_item_id',
				'NavigationMenu.created_date',
				'NavigationMenu.modified_date',
				'NavigationMenu.data',
			),
			'conditions' => array(
				'NavigationMenu.lang_id' => $lang_id,
			),
			'order' => array(
				'NavigationMenu.modified_date DESC',
			),
		));
		foreach($target_nav_menu as $nav_menu){
			for($i=0;$i<count($ret);$i++){
				if( ($ret[$i]['modified_date'] == '0000-00-00 00:00:00') &&
						($ret[$i]['menu_item_id'] == $nav_menu['NavigationMenu']['menu_item_id'])) {
					$ret[$i]['created_date'] = $nav_menu['NavigationMenu']['created_date'];
					$ret[$i]['modified_date'] = $nav_menu['NavigationMenu']['modified_date'];
					$ret[$i]['data'] = $nav_menu['NavigationMenu']['data'];
				}
			}
		}

/*
		// ローカライズされた値を取得できなかったアイテムを抽出
		$lacking_items = array();
		for($i=0;$i<count($ret);$i++){
			if(($ret[$i]['modified_date'] == '0000-00-00 00:00:00')){
				$lacking_item = array(
					'nav_index' => $i,
					'menu_item_id' => $ret[$i]['menu_item_id'],
				);
				$lacking_items[] = $lacking_item;
			}
		}

		// ローカライズされていない部分に対し、他の言語から値を取得する
		$other_nav_menu = $this->find('all', array(
			'fields' => array(
				'NavigationMenu.menu_item_id',
				'NavigationMenu.created_date',
				'NavigationMenu.modified_date',
				'NavigationMenu.data',
			),
			'order' => array(
				'NavigationMenu.lang_id ASC',			// 国順にまず見る
				'NavigationMenu.modified_date DESC',
			),
		));
		for($i=0;$i<count($lacking_items);$i++){
			foreach($other_nav_menu as $nav_menu){
				if($nav_menu['NavigationMenu']['menu_item_id'] == $lacking_items[$i]['menu_item_id']){
					$ret[ $lacking_items[$i]['nav_index'] ]['ref_data'] = $nav_menu['NavigationMenu']['data'];
					break;
				}
			}
		}
*/
		// 英語のリファレンスデータを取得する
		$ref_nav_menu = $this->find('all', array(
			'fields' => array(
				'NavigationMenu.menu_item_id',
				'NavigationMenu.created_date',
				'NavigationMenu.modified_date',
				'NavigationMenu.data',
				'NavigationMenu.disabled',
			),
			'order' => array(
				'NavigationMenu.lang_id ASC',			// 国順にまず見る
				'NavigationMenu.modified_date DESC',
			),
		));
		for($i=0;$i<count($ret);$i++){
			foreach($ref_nav_menu as $nav_menu){
				if($ret[$i]['menu_item_id'] == $nav_menu['NavigationMenu']['menu_item_id']){
					$ret[$i]['disabled'] = $nav_menu['NavigationMenu']['disabled'];
					$ret[$i]['ref_data'] = $nav_menu['NavigationMenu']['data'];
					break;
				}
			}
		}

		return $ret;
	}

}
