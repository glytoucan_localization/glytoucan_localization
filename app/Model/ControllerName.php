<?php
class ControllerName extends AppModel{
	public $name = 'controller_name';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'controller_names';

	// リスト表示するフィールドを指定
	public $displayField = 'name';

	public $validate = array();

}
