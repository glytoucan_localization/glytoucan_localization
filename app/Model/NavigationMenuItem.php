<?php
class NavigationMenuItem extends AppModel{
	public $name = 'navigation_menu_item';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'navigation_menu_items';

	// リスト表示するフィールドを指定
	public $displayField = 'name';

	public $validate = array();

}
