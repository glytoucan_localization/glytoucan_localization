<?php
class Article extends AppModel{
	public $name = 'article';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'articles';

	// アソシエーションの設定
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'Language' => array(
			'className' => 'Language',
			'foreignKey' => 'lang_id'
		),
		'ViewName' => array(
			'className' => 'ViewName',
			'foreignKey' => 'view_id'
		),
		'Place' => array(
			'className' => 'Place',
			'foreignKey' => 'place_id'
		),
	);

	public $validate = array();

	public function get_place_data($lang_id, $view_id){
		$ret = array();

		// Place一覧の取得
		$places = $this->Place->find('all', array('order' => 'Place.id ASC'));
		foreach($places as $place){
			$place_data = array();
			$place_data['place_id'] = $place['Place']['id'];
			$place_data['place_name'] = $place['Place']['name'];
			$place_data['max_num'] = 0;
			$place_data['articles'] = array();
			$ret[] = $place_data;
		}

		// 既に登録されているArticle情報から、言語をまたいで各Placeごとの最大Num数を取得
		// (group句を使い、virtualFields, Article__を使用した特殊記法 下記参照)
		// http://www.ah-2.com/2012/02/19/cakephp_virtual_fields.html
		$this->virtualFields['max_num'] = 0;	// Add a virtual field
		$nums = $this->find('all', array(
			'fields' => array(
				'Article.place_id',
				'MAX( Article.num ) as Article__max_num' ,	// Use the Article__ alias
			),
			'conditions' => array(
				'Article.view_id' => $view_id,
			),
			'group' => array(
				'Article.place_id'
			),
			'order' => array(
				'Article.id ASC',
			),
		));
		foreach($nums as $num){
			for($i=0;$i<count($ret);$i++){
				if($num['Article']['place_id'] == $ret[$i]['place_id']){
					$ret[$i]['max_num'] = $num['Article']['max_num'];
				}
			}
		}

		// 指定されたlang_id, view_idを満たす、データベースに格納されているArticleデータを取得
		// 古いデータを読まないよう、新しいデータが先に読み込まれるようにorderを設定する
		$target_articles = $this->find('all', array(
			'fields' => array(
				'Article.place_id',
				'Article.num',
				'Article.created_date',
				'Article.modified_date',
				'Article.article',
			),
			'conditions' => array(
				'Article.lang_id' => $lang_id,
				'Article.view_id' => $view_id,
			),
			'order' => array(
				'Article.modified_date DESC',
				'Article.id ASC',
			)
		));

		// 取得したArticleデータを返却用変数にセット
		foreach($target_articles as $article){
			for($i=0;$i<count($ret);$i++){
				// 古いデータを読み込まないように値が既にセットされていたらスキップさせる
				if($article['Article']['place_id'] == $ret[$i]['place_id'] && 
					 !isset($ret[$i]['articles'][ $article['Article']['num'] ]) ){
					$article_data = array();
					$article_data['num'] = $article['Article']['num'];
					$article_data['created_date'] = $article['Article']['created_date'];
					$article_data['modified_date'] = $article['Article']['modified_date'];
					$article_data['article'] = $article['Article']['article'];
					$article_data['ref_article'] = "";
					$article_data['disabled'] = -1;
					$ret[$i]['articles'][ $article_data['num'] ] = $article_data;
				}
			}
		}

/*
		// その言語のそのViewのそのPlaceの情報としてはまだArticleにデータがないPlaceとnumを収集する
		$lacking_place_nums = array();
		for($i=0;$i<count($ret);$i++){
			$lacking_place_num = array(
				'place_id' => $ret[$i]['place_id'],
				'nums' => null
			);
			if( $ret[$i]['max_num'] != count($ret[$i]['articles']) ){
				// max_numの値と登録されているarticleの数が不一致(翻訳されていないnumが存在する)
				$total_nums = array();
				for($j=1;$j<=$ret[$i]['max_num'];$j++){
					$total_nums[] = $j;
				}
				$registered_nums = array();
				foreach($ret[$i]['articles'] as $registered_article){
					$registered_nums[] = $registered_article['num'];
				}
				$unregistered_nums = array_diff($total_nums, $registered_nums);
				$lacking_place_num['nums'] = array_merge($unregistered_nums);		// 配列のインデックスを0から振り直させる
			}
			$lacking_place_nums[] = $lacking_place_num;
		}

		// 欠損しているPlaceデータのうち、他言語で既にArticleがあるものを取得する
		$other_lang_data = $this->find('all', array(
			'fields' => array(
				'Article.lang_id',
				'Article.place_id',
				'Article.num',
				'Article.article',
			),
			'conditions' => array(
				'Article.view_id' => $view_id,
			),
			'order' => array(
				'Article.lang_id ASC',			// 国順にまず見る
				'Article.modified_date DESC',
				'Article.place_id ASC',
				'Article.num ASC',
				'Article.id ASC',
			)
		));

		// 欠損しているPlaceデータのうち、他の言語で記述があったものをセットする
		for($i=0;$i<count($lacking_place_nums);$i++){
			if($lacking_place_nums[$i]['nums'] != null){
				for($j=0;$j<count($lacking_place_nums[$i]['nums']);$j++){
					// 不足している対象の情報
					$target_place_id = $lacking_place_nums[$i]['place_id'];
					$target_num = $lacking_place_nums[$i]['nums'][$j];

					for($k=0;$k<count($other_lang_data);$k++){
						// 不足している情報に対し、他の言語で該当データが見つかった場合
						if( $target_place_id == $other_lang_data[$k]['Article']['place_id'] &&
							  $target_num == $other_lang_data[$k]['Article']['num'] ){

							$article_data = array();
							$article_data['num'] = $target_num;
							$article_data['created_date'] = '0000-00-00 00:00:00';
							$article_data['modified_date'] = '0000-00-00 00:00:00';
							$article_data['article'] = $other_lang_data[$k]['Article']['article'];
							$ret[$i]['articles'][$target_num] = $article_data;

							$k = count($other_lang_data);
						}
					}

					// 対象データが他の言語でも見つからなかった場合
					if($ret[$i]['articles'][$target_num] == null){
						// データベースの値が改竄されなければ、このブロックは実行されない
						$article_data = array();
						$article_data['num'] = $target_num;
						$article_data['created_date'] = '0000-00-00 00:00:00';
						$article_data['modified_date'] = '0000-00-00 00:00:00';
						$article_data['article'] = '';
						$ret[$i]['articles'][$target_num] = $article_data;
					}
				}
			}
		}
*/
		// 英語のリファレンスデータを取得する
		$all_lang_data = $this->find('all', array(
			'fields' => array(
				'Article.lang_id',
				'Article.place_id',
				'Article.num',
				'Article.article',
				'Article.disabled',
			),
			'conditions' => array(
				'Article.view_id' => $view_id,
			),
			'order' => array(
				'Article.lang_id ASC',			// 国順にまず見る
				'Article.modified_date DESC',
				'Article.place_id ASC',
				'Article.num ASC',
				'Article.id ASC',
			)
		));
		foreach($all_lang_data as $article){
			for($i=0;$i<count($ret);$i++){
				// 古いデータを読み込まないように値が既にセットされていたらスキップさせる
				if($article['Article']['place_id'] == $ret[$i]['place_id']){
					if( !isset($ret[$i]['articles'][ $article['Article']['num'] ]) ){
						$article_data = array();
						$article_data['num'] = $article['Article']['num'];
						$article_data['created_date'] = '0000-00-00 00:00:00';
						$article_data['modified_date'] = '0000-00-00 00:00:00';
						$article_data['article'] = "";
						$article_data['ref_article'] = $article['Article']['article'];
						$article_data['disabled'] = $article['Article']['disabled'];
						$ret[$i]['articles'][ $article_data['num'] ] = $article_data;

					}else if($ret[$i]['articles'][ $article['Article']['num'] ]['disabled'] === -1){
						$ret[$i]['articles'][ $article['Article']['num'] ]['disabled'] = $article['Article']['disabled'];

						$ret[$i]['articles'][ $article['Article']['num'] ]['ref_article'] = $article['Article']['article'];
					}
				}
			}
		}

		// 配列の添字でソートする(キーが、2,1,4,3のようにバラバラになっても、キーを再度1,2,3,4になるように振り直す)
		for($i=0;$i<count($ret);$i++){
			ksort($ret[$i]['articles']);
		}

		return $ret;
	}
}
