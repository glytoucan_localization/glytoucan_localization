<?php
class User extends AppModel{
	public $name = 'user';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'users';

	// データベースに格納する値のバリデーション指定
	public $validate = array(

		'name' => array(
			array(
				'rule' => 'notEmpty',
				'message' => 'Account name is required.'
			),
			array(
				'rule' => 'alphaNumeric',
				'message' => 'Only alphabets or numbers are permitted.'
			),
			array(
				'rule' => array('maxLength', 20),
				'message' => 'Account name is too long.'
			),
		),
		'password' => array(
			array(
				'rule' => 'notEmpty',
				'message' => 'Password is required.'
			),
			array(
				'rule' => array('between', 4, 20),
				'message' => 'Password must be 4 - 20 characters.'
			),
		),
	);

	// 保存前の処理
	function beforeSave($options = array()){
		// パスワードが入力されている際には暗号化を行う
		if(!empty($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
	}
}
