<?php
class ChangeLog extends AppModel{
	public $name = 'change_log';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'change_logs';

	// リスト表示するフィールドを指定
	//public $displayField = 'name';

	// アソシエーションの設定
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
		),
		'ContentType' => array(
			'className' => 'ContentType',
			'foreignKey' => 'content_type_id'
		),
		'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'target_id'
		),
		'NavigationMenu' => array(
			'className' => 'NavigationMenu',
			'foreignKey' => 'target_id'
		),
		'Common' => array(
			'className' => 'Common',
			'foreignKey' => 'target_id'
		),
	);

	public $validate = array();

	public function get_change_logs($log_num=10){
		return $this->find('all', array(
			'fields' => array(
				'ChangeLog.date',
				'ChangeLog.content_type_id',
				'Article.lang_id',
				'Article.view_id',
				'Article.place_id',
				'Article.num',
				'NavigationMenu.lang_id',
				'NavigationMenu.menu_item_id',
				'Common.lang_id',
				'Common.item_id',
				'User.name',
			),
			'order' => array(
				'ChangeLog.date DESC',
				'ChangeLog.id ASC',
			),
			'conditions' => array(
				'OR' => array(
					array(
						'ContentType.name' => 'article',
						'Article.id not' => null,
					),
					array(
						'ContentType.name' => 'navigation_menu',
						'NavigationMenu.id not' => null,
					),
					array(
						'ContentType.name' => 'common',
						'Common.id not' => null,
					),
				),
			),
			'limit' => $log_num,
			'recursive' => 3,
		));
	}
}
