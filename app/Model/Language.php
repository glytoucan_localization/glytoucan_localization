<?php
class Language extends AppModel{
	public $name = 'language';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'languages';

	// リスト表示するフィールドを指定
	public $displayField = 'name';

	public $validate = array();

}
