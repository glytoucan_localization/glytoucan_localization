<?php
class CommonItem extends AppModel{
	public $name = 'common_item';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'common_items';

	// リスト表示するフィールドを指定
	public $displayField = 'name';

	public $validate = array();

}
