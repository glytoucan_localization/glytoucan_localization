<?php
class ViewName extends AppModel{
	public $name = 'view_name';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'view_names';

	// リスト表示するフィールドを指定
	public $displayField = 'name';

	// アソシエーションの設定
	public $belongsTo = array(
		'ControllerName' => array(
			'className' => 'ControllerName',
			'foreignKey' => 'cont_id'
		),
	);

	public $validate = array();

}
