<?php
class Place extends AppModel{
	public $name = 'place';

	// database.phpに記載しているデータベース設定のうち、どれを使用するか
	public $useDbConfig = 'mysql';

	// 使用するテーブルを指定する
	public $useTable = 'places';

	// リスト表示するフィールドを指定
	public $displayField = 'name';

	public $validate = array();

}
