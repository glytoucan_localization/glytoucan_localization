
<div class="row">
	<div class="col col-md-4">
<?php if($auth->loggedIn()): ?>
		<?php echo $this->element('tree_explorer', array(
			'isAdmin' => $isAdmin,
			'menu' => $tree,
		)); ?>
<?php else: ?>
		<?php echo $this->element('login_form'); ?>
<?php endif; ?>

	</div>
	<div class="col col-md-8">

		<div class="col col-md-12">
			<h1>GlyTouCan Localization Form</h1>
			<hr>

			<div class="panel panel-default">
				<div class="panel-body">
					<p>
						Language: <strong><?php echo $languageList[ $view_data['selected_lang_id'] ];?></strong>
					</p>
					<p>
						Page: <strong>Navigation Menu</strong>
					</p>
				</div>
			</div>

<?php echo $this->Form->create('NavigationMenu', array(
	'name' => 'localization_navigation_menu_submit_form',
	'url' => '/localizations/navigation_menu',
	'inputDefaults' => array(
		'div' => 'form-group',
		'label' => array(
			'class' => 'col col-md-5 control-label'
		),
		'wrapInput' => 'col col-md-7',
		'class' => 'form-control'
	),
	//'class' => 'well'
	'class' => 'form-horizontal'
)); ?>
			<fieldset>

				<?php echo $this->Form->input('selected_lang_id', array(
					'type' => 'hidden',
					'class' => 'form-control',
					'value' => $view_data['selected_lang_id'],
				)); ?>
				<?php echo $this->Form->input('data_submit', array(
					'type' => 'hidden',
					'class' => 'form-control',
					'value' => 1,
				)); ?>

<?php if(isset($submit_errors) == true): ?>
	<?php foreach($submit_errors as $submit_error): ?>
		<?php if(isset($submit_error['error']) == true): ?>
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>Notice!</strong> The new common item name has already used.
				</div>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif ?>

<?php if(isset($submit_results) == true): ?>
	<?php if(count($submit_results) != 0): ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>Success!</strong> Your submit has been accepted successfully.
		<?php foreach($submit_results as $submit_result): ?>
					<p>
			<?php if(isset($submit_result['added_menu_item'])==true): ?>
						<?php echo $submit_result['added_menu_item'];?> was added.
			<?php else: ?>
						<?php echo $languageList[ $submit_result['lang_id'] ];?>/NavigationMenu/<?php echo $navigationMenuItemList[ $submit_result['menu_item_id'] ];?>
			<?php endif; ?>
					</p>
		<?php endforeach; ?>

				</div>

	<?php else: ?>
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>Notice!</strong> No text change was detected. The database wasn't changed.
				</div>

	<?php endif; ?>
<?php endif; ?>

				<div class="row">
					<div class="col col-md-12">
						<button class="btn btn-success col-md-4 col-md-offset-8" type="button" id="expand_btn" data-toggle="button">Expand All / Hide All</button>
					</div>
				</div>
				<hr>

				<div class="panel-group" id="menu_accordion">
				

<?php foreach($view_data['items'] as $item_idx => $nav_item): ?>

	<?php if($nav_item['disabled'] != 0): ?>
					<?php if($isAdmin == false): ?>
						<?php continue; ?>
					<?php endif; ?>
					<div class="panel panel-danger">
	<?php elseif($nav_item['modified_date'] != '0000-00-00 00:00:00'): //既入力済 ?>
					<div class="panel panel-success">
	<?php elseif($nav_item['ref_data'] != ''): //未翻訳 ?>
					<div class="panel panel-warning">
	<?php else: //未使用 ?>
					<?php if($isAdmin == false): ?>
						<?php continue; ?>
					<?php endif; ?>
					<div class="panel panel-danger">
	<?php endif; ?>


						<div class="panel-heading">
							<h4 class="panel-title">
								<?php echo '<a data-toggle="collapse" data-parent="#menu_accordion" href="#collapse_'.$item_idx.'">'."\n"; ?>
									<?php echo $nav_item['menu_item_name'];?>
								</a>
							</h4>
						</div>

						<?php echo '<div id="collapse_'.$item_idx.'" class="panel-collapse collapse">'."\n"; ?>
							<div class="panel-body">

	<?php if($nav_item['ref_data'] == ''): //英語リファレンスなし ?>
		<?php if($isAdmin == true): ?>
			<?php if($view_data['selected_lang_id'] == 1): ?>
								<div class="col col-md-12">
									<?php echo '<p><strong>Modified Date: '.$nav_item['modified_date'].'&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;'.'Created Date: '.$nav_item['created_date'].'</strong></p>'; ?>
									<?php echo '<p><strong>Input the text for "'.$nav_item['menu_item_name'].'".</strong></p>'."\n"; ?>
								</div>

			<?php if(($isAdmin == true) && ($nav_item['modified_date'] != '0000-00-00 00:00:00')): ?>
								<div class="col col-md-12">
									<?php echo $this->Form->input('disable-nav_'.$nav_item['menu_item_id'], array(
										'type' => 'checkbox',
										'div' => false,
										'label' => array(
											'text' => "Disable this item",
											'class' => false,
										),
										'class' => false,
										'wrapInput' => false,
										($nav_item['disabled']) ? 'checked' : '',
									)); ?>
								</div>
			<?php endif; ?>

								<div class="col col-md-12">
									<?php echo $this->Form->input('nav_'.$nav_item['menu_item_id'], array(
										'type' => 'textarea',
										'label' => false,
										'placeholder' => 'Input translated text for "'.$nav_item['menu_item_name'].'" here...',
										'value' => $nav_item['data'],
										'class' => 'form-control',
										'wrapInput' => 'col col-md-12',
										'rows' => 3
									)); ?>
								</div>

			<?php else: ?>
									<div class="col col-md-12">
										<div class="alert alert-danger" role="alert">
											This place of the page is not used. To add new texts, please refer to the English version page.
										</div>
									</div>
			<?php endif; ?>
		<?php endif; ?>

	<?php elseif($nav_item['data'] == ''): //まだその言語の翻訳データなし ?>

		<?php if($view_data['selected_lang_id'] == 1): ?>
								<div class="col col-md-12">
									<?php echo '<p><strong>Modified Date: '.$nav_item['modified_date'].'&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;'.'Created Date: '.$nav_item['created_date'].'</strong></p>'; ?>
									<?php echo '<p><strong>Input the text for "'.$nav_item['menu_item_name'].'".</strong></p>'."\n"; ?>
								</div>

								<div class="col col-md-12">
									<?php echo $this->Form->input('nav_'.$nav_item['menu_item_id'], array(
										'type' => 'textarea',
										'label' => false,
										'placeholder' => 'Input translated text for "'.$nav_item['menu_item_name'].'" here...',
										'value' => $nav_item['data'],
										'class' => 'form-control',
										'wrapInput' => 'col col-md-12',
										'rows' => 3
									)); ?>
								</div>

		<?php else: ?>

			<?php if($nav_item['disabled'] != 0): ?>
								<div class="col col-md-12">
									<div class="alert alert-danger" role="alert">
										This place of the page is not used. To add new texts, please refer to the English version page.
									</div>
								</div>
			<?php else: ?>

								<?php echo '<p><strong>Modified Date: '.$nav_item['modified_date'].'&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;'.'Created Date: '.$nav_item['created_date'].'</strong></p>'; ?>
								<?php echo '<p><strong>Input translated text for "'.$nav_item['menu_item_name'].'".</strong></p>'."\n"; ?>

								<p><strong><span style="color: red">This text hasn't been translated into this language yet.</span></strong></p>

								<div class="row">
									<div class="col col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												English Text
											</div>
											<div class="panel-body">
												<div style="margin-left:5px;margin-right:5px;">
													<div class="form-group">
														<?php echo $this->Form->input('eng-nav_'.$nav_item['menu_item_id'], array(
															'type' => 'textarea',
															'label' => false,
															'placeholder' => 'Input translated text for "'.$nav_item['menu_item_name'].'" here...',
															'value' => $nav_item['ref_data'],
															'class' => 'form-control',
															'wrapInput' => 'col col-md-12',
															'readonly',
															'rows' => 4
														)); ?>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												Translated Text
											</div>
											<div class="panel-body">
												<div style="margin-left:5px;margin-right:5px;">
													<div class="form-group">
														<?php echo $this->Form->input('nav_'.$nav_item['menu_item_id'], array(
															'type' => 'textarea',
															'label' => false,
															'placeholder' => 'Input translated text for "'.$nav_item['menu_item_name'].'" here...',
															'value' => $nav_item['data'],
															'class' => 'form-control',
															'wrapInput' => 'col col-md-12',
															'rows' => 4
														)); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
			<?php endif; ?>
		<?php endif; ?>

	<?php else: // その言語の翻訳データあり ?>
		<?php if($view_data['selected_lang_id'] == 1): ?>
								<div class="col col-md-12">
									<?php echo '<p><strong>Modified Date: '.$nav_item['modified_date'].'&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;'.'Created Date: '.$nav_item['created_date'].'</strong></p>'; ?>
									<?php echo '<p><strong>Input the text for "'.$nav_item['menu_item_name'].'".</strong></p>'."\n"; ?>
								</div>

			<?php if($isAdmin == true): ?>
								<div class="col col-md-12">
									<?php echo $this->Form->input('disable-nav_'.$nav_item['menu_item_id'], array(
										'type' => 'checkbox',
										'div' => false,
										'label' => array(
											'text' => "Disable this item",
											'class' => false,
										),
										'class' => false,
										'wrapInput' => false,
										($nav_item['disabled']) ? 'checked' : '',
									)); ?>
								</div>
			<?php endif; ?>

								<div class="col col-md-12">
									<?php echo $this->Form->input('nav_'.$nav_item['menu_item_id'], array(
										'type' => 'textarea',
										'label' => false,
										'placeholder' => 'Input translated text for "'.$nav_item['menu_item_name'].'" here...',
										'value' => $nav_item['data'],
										'class' => 'form-control',
										'wrapInput' => 'col col-md-12',
										'rows' => 3
									)); ?>
								</div>

		<?php else: ?>
			<?php if($nav_item['disabled'] != 0): ?>
								<div class="col col-md-12">
									<div class="alert alert-danger" role="alert">
										This place of the page is not used. To add new texts, please refer to the English version page.
									</div>
								</div>
			<?php else: ?>

								<?php echo '<p><strong>Modified Date: '.$nav_item['modified_date'].'&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;'.'Created Date: '.$nav_item['created_date'].'</strong></p>'."\n"; ?>
								<?php echo '<p><strong>Input translated text for "'.$nav_item['menu_item_name'].'".</strong></p>'."\n"; ?>

								<div class="row">
									<div class="col col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												English Text
											</div>
											<div class="panel-body">
												<div style="margin-left:5px;margin-right:5px;">
													<div class="form-group">
														<?php echo $this->Form->input('eng-nav_'.$nav_item['menu_item_id'], array(
															'type' => 'textarea',
															'label' => false,
															'placeholder' => 'Input translated text for "'.$nav_item['menu_item_name'].'" here...',
															'value' => $nav_item['ref_data'],
															'class' => 'form-control',
															'wrapInput' => 'col col-md-12',
															'readonly',
															'rows' => 4
														)); ?>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												Translated Text
											</div>
											<div class="panel-body">
												<div style="margin-left:5px;margin-right:5px;">
													<div class="form-group">
														<?php echo $this->Form->input('nav_'.$nav_item['menu_item_id'], array(
															'type' => 'textarea',
															'label' => false,
															'placeholder' => 'Input translated text for "'.$nav_item['menu_item_name'].'" here...',
															'value' => $nav_item['data'],
															'class' => 'form-control',
															'wrapInput' => 'col col-md-12',
															'rows' => 4
														)); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
			<?php endif; ?>
		<?php endif; ?>

	<?php endif; ?>

							</div>
						</div>
					</div>
<?php endforeach; ?>

				</div>
				
<?php if(($isAdmin == true) && ($view_data['selected_lang_id'] == 1)): ?>
				<div class="row">
					<div class="col col-md-12">
						<p><strong><span style="color: red">Input new menu item name.</span></strong></p>
					</div>
					<div class="col col-md-12">
						<?php echo $this->Form->input('new-nav', array(
							'type' => 'text',
							'label' => false,
							'placeholder' => 'Input new menu item name here...',
							'class' => 'form-control',
							'wrapInput' => 'col col-md-12',
						)); ?>
					</div>
				</div>
<?php endif; ?>

				<div class="row" style="padding-bottom:50px;">
					<div class="col col-md-12">
						<?php echo $this->Form->submit('Submit', array(
							'div' => false,
							'class' => 'btn btn-primary col-md-offset-9 col-md-3'
						)); ?>
					</div>
				</div>
			</fieldset>
<?php echo $this->Form->end(); ?>

		</div>
	</div>
</div>
