
<div class="row">
	<div class="col col-md-4">
<?php if($auth->loggedIn()): ?>
		<?php echo $this->element('tree_explorer', array(
			'isAdmin' => $isAdmin,
			'menu' => $tree,
		)); ?>
<?php else: ?>
		<?php echo $this->element('login_form'); ?>
<?php endif; ?>

	</div>
	<div class="col col-md-8">

		<div class="col col-md-12">
			<h1>GlyTouCan Localization Form</h1>
			<hr>

			<div class="panel panel-default">
				<div class="panel-body">
					<p>
						Language: <strong><?php echo $languageList[ $view_data['selected_lang_id'] ];?></strong>
					</p>
					<p>
						URL: <strong>glytoucan.org/<?php echo $controllerNameList[ $view_data['selected_cont_name_id'] ];?>/<?php echo $viewNameList[ $view_data['selected_view_name_id'] ];?></strong>
					</p>
				</div>
			</div>

<?php echo $this->Form->create('Article', array(
	'name' => 'localization_article_submit_form',
	'url' => '/localizations/article',
	'inputDefaults' => array(
		'div' => 'form-group',
		'label' => false,
		//'label' => array(
		//	'class' => 'col col-md-3 control-label'
		//),
		//'wrapInput' => 'col col-md-9',
		'class' => 'form-control'
	),
	//'class' => 'well'
	'class' => 'form-horizontal'
)); ?>
			<fieldset>

				<?php echo $this->Form->input('selected_lang_id', array(
					'type' => 'hidden',
					'class' => 'form-control',
					'value' => $view_data['selected_lang_id'],
				)); ?>
				<?php echo $this->Form->input('selected_cont_name_id', array(
					'type' => 'hidden',
					'class' => 'form-control',
					'value' => $view_data['selected_cont_name_id'],
				)); ?>
				<?php echo $this->Form->input('selected_view_name_id', array(
					'type' => 'hidden',
					'class' => 'form-control',
					'value' => $view_data['selected_view_name_id'],
				)); ?>
				<?php echo $this->Form->input('data_submit', array(
					'type' => 'hidden',
					'class' => 'form-control',
					'value' => 1,
				)); ?>

<?php if(isset($submit_results) == true): ?>
<?php if(count($submit_results) != 0): ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>Success!</strong> Your submit has been accepted successfully.
<?php foreach($submit_results as $submit_result): ?>
					<p>
						<?php echo $languageList[ $submit_result['lang_id'] ];?>/<?php echo $controllerNameList[ $submit_result['cont_name_id'] ];?>/<?php echo $viewNameList[ $submit_result['view_name_id'] ];?>/<?php echo $placeList[ $submit_result['place_id'] ];?>/Num.<?php echo $submit_result['num'];?>
					</p>

<?php endforeach; ?>

				</div>

<?php else: ?>
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert">
						<span aria-hidden="true">&times;</span>
						<span class="sr-only">Close</span>
					</button>
					<strong>Notice!</strong> No text change was detected. The database wasn't changed.
				</div>

<?php endif; ?>
<?php endif; ?>

				<div class="row">
					<div class="col col-md-12">
						<button class="btn btn-success col-md-4 col-md-offset-8" type="button" id="expand_btn" data-toggle="button">Expand All / Hide All</button>
					</div>
				</div>
				<hr>

				<div class="panel-group" id="place_accordion">

<?php foreach($view_data['places'] as $place_idx => $place): ?>
	<?php
	$d_flag = true;
	foreach($place['articles'] as $article) {
		if($article['disabled'] == 0){
			$d_flag = false;
		}
	}
	$m_flag = true;
	foreach($place['articles'] as $article) {
		if($article['modified_date'] == '0000-00-00 00:00:00' && $article['disabled'] != 1){
			$m_flag = false;
		}
	}
	?>

	<?php if($place['max_num'] != 0): ?>
		<?php if($d_flag == true): ?>
					<?php if($isAdmin == false): ?>
						<?php continue; ?>
					<?php endif; ?>
					<div class="panel panel-danger">

		<?php elseif($m_flag == true): ?>
					<div class="panel panel-success">

		<?php else: ?>
					<div class="panel panel-warning">

		<?php endif; ?>
	<?php else: ?>
					<?php if($isAdmin == false): ?>
						<?php continue; ?>
					<?php endif; ?>
					<div class="panel panel-danger">

	<?php endif; ?>


						<div class="panel-heading">
							<h4 class="panel-title">
								<?php echo '<a data-toggle="collapse" data-parent="#place_accordion" href="#collapse_'.$place_idx.'">'."\n"; ?>
									<?php echo $languageList[ $view_data['selected_lang_id'] ];?> / <?php echo $controllerNameList[ $view_data['selected_cont_name_id'] ];?> / <?php echo $viewNameList[ $view_data['selected_view_name_id'] ];?> / <?php echo $placeList[ $place['place_id'] ];?>
								</a>
							</h4>
						</div>

						<?php echo '<div id="collapse_'.$place_idx.'" class="panel-collapse collapse">'."\n"; ?>
							<div class="panel-body">

	<?php if(($d_flag == true) && ($view_data['selected_lang_id'] != 1)): ?>
								<div class="alert alert-danger" role="alert">
									This place of the page is not used. To add new texts, please refer to the English version page.
								</div>

	<?php elseif(($place['max_num'] == 0) && ($view_data['selected_lang_id'] != 1)): ?>
								<div class="alert alert-danger" role="alert">
									This place of the page is not used. To add new texts, please refer to the English version page.
								</div>

	<?php else: ?>
		<?php foreach($place['articles'] as $article): ?>

			<?php if(($article['disabled'] != 0) && (($view_data['selected_lang_id'] != 1) || ($isAdmin != true))): ?>
								<div class="alert alert-danger" role="alert">
									<?php echo $placeList[ $place['place_id'] ].'" No.'.($article['num']); ?> is disabled now.
								</div>

			<?php else: ?>
								<?php echo '<p><strong>Modified Date: '.$article['modified_date'].'&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;'.'Created Date: '.$article['created_date'].'</strong></p>'."\n"; ?>

				<?php if($view_data['selected_lang_id'] == 1): ?>
								<?php echo '<p><strong>Input the text for the place "'.$placeList[ $place['place_id'] ].'" No.'.($article['num']).'.</strong></p>'."\n"; ?>
				<?php else: ?>
								<?php echo '<p><strong>Input translated text for the place "'.$placeList[ $place['place_id'] ].'" No.'.($article['num']).'.</strong></p>'."\n"; ?>
				<?php endif; ?>

				<?php if($article['modified_date']=='0000-00-00 00:00:00'): ?>
								<p><strong><span style="color: red">This text hasn't been translated into this language yet.</span></strong></p>
				<?php endif; ?>

				<?php if(($isAdmin == true) and ($view_data['selected_lang_id'] == 1)): ?>
								<div class="col col-md-12">
									<?php echo $this->Form->input('disable-art_'.$place['place_id'].'_'.$article['num'], array(
										'type' => 'checkbox',
										'div' => false,
										'label' => array(
											'text' => "Disable this item",
											'class' => false,
										),
										'class' => false,
										'wrapInput' => false,
										($article['disabled']) ? 'checked' : '',
									)); ?>
								</div>
				<?php endif; ?>

				<?php if($view_data['selected_lang_id'] == 1): ?>
								<div class="col col-md-12" style="padding-bottom:10px;">
									<?php echo $this->Form->input('art_'.$place['place_id'].'_'.$article['num'], array(
										'type' => 'textarea',
										'label' => false,
										'placeholder' => 'Input the text for the place "'.$placeList[ $place['place_id'] ].'" No.'.($article['num']).' here...',
										'class' => 'form-control',
										'wrapInput' => 'col col-md-12',
										'value' => $article['article'],
										'rows' => 10
									))."\n"; ?>
								</div>

				<?php else: ?>
								<div class="row" style="padding-bottom:20px;">
									<div class="col col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												English Text
											</div>
											<div class="panel-body">
												<div style="margin-left:5px;margin-right:5px;">
													<div class="form-group">
														<?php echo $this->Form->input('eng-art_'.$place['place_id'].'_'.$article['num'], array(
															'type' => 'textarea',
															'label' => false,
															'placeholder' => 'Input translated text for the place "'.$placeList[ $place['place_id'] ].'" No.'.($article['num']).' here...',
															'value' => $article['ref_article'],
															'class' => 'form-control',
															'wrapInput' => 'col col-md-12',
															'readonly',
															'rows' => 10
														)); ?>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="col col-md-6">
										<div class="panel panel-primary">
											<div class="panel-heading">
												Translated Text
											</div>
											<div class="panel-body">
												<div style="margin-left:5px;margin-right:5px;">
													<div class="form-group">
														<?php echo $this->Form->input('art_'.$place['place_id'].'_'.$article['num'], array(
															'type' => 'textarea',
															'label' => false,
															'placeholder' => 'Input translated text for the place "'.$placeList[ $place['place_id'] ].'" No.'.($article['num']).' here...',
															'value' => $article['article'],
															'class' => 'form-control',
															'wrapInput' => 'col col-md-12',
															'rows' => 10
														)); ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
				<?php endif; ?>

			<?php endif; ?>

		<?php endforeach; ?>

		<?php if(($isAdmin == true) && ($view_data['selected_lang_id'] == 1)): ?>
								<div class="col col-md-12" style="padding-bottom:10px;">
									<?php echo $this->Form->input('new-art_'.$place['place_id'].'_'.($place['max_num']+1), array(
										'type' => 'textarea',
										'label' => array(
											'text' => '<span style="color: red">Input the text for the "NEW" place "'.$placeList[ $place['place_id'] ].'" No.'.($place['max_num']+1).'.</span>',
										),
										'placeholder' => 'Input the text for the "NEW" place "'.$placeList[ $place['place_id'] ].'" No.'.($place['max_num']+1).' here...',
										'class' => 'form-control',
										'wrapInput' => 'col col-md-12',
										'rows' => 10
									))."\n"; ?>
								</div>

		<?php endif; ?>

	<?php endif; ?>

							</div>
						</div>
					</div>
<?php endforeach; ?>

				</div>

				<div class="row" style="padding-bottom:50px;">
					<div class="col col-md-12">
						<?php echo $this->Form->submit('Submit', array(
							'div' => false,
							'class' => 'btn btn-primary col-md-offset-9 col-md-3'
						)); ?>
					</div>
				</div>
			</fieldset>
<?php echo $this->Form->end(); ?>

		</div>
	</div>
</div>
