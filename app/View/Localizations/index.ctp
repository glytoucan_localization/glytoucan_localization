
<div class="row">
	<div class="col col-md-4">
<?php if($auth->loggedIn()): ?>
		<?php echo $this->element('tree_explorer', array(
			'isAdmin' => $isAdmin,
			'menu' => $tree,
		)); ?>
<?php else: ?>
		<?php echo $this->element('login_form'); ?>
<?php endif; ?>

	</div>
	<div class="col col-md-8">
		<div class="col col-md-12">
			<h1>GlyTouCan Localization Form</h1>
			<hr>
			<div class="jumbotron">
				<h2>Welcome to GlyTouCan Localization Form!</h2>
				<p>
					This is GlyTouCan Localization Form page for the limited registered users.
				</p>
<?php if($auth->loggedIn()): ?>
				<p>
					<strong>Please select the page you want to edit from the left tree.</strong>
				</p>
<?php else: ?>
				<p>
					<strong>If you are a registered user, please login from the left form first.</strong>
				</p>
<?php endif; ?>
			</div>

<?php $admin_operation = $this->Session->flash('admin_operation'); ?>
<?php if($admin_operation == 'success'): ?>
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<strong>Success!</strong> <?php echo $this->Session->flash('admin_operation_message'); ?>
			</div>
<?php elseif($admin_operation == 'error'): ?>
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<strong>Error!</strong> <?php echo $this->Session->flash('admin_operation_message'); ?>
			</div>
<?php endif; ?>

<?php $password_changed = $this->Session->flash('password_changed'); ?>
<?php if(!empty($password_changed)): ?>
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<strong>Success!</strong> Your password was changed successfully.
			</div>
<?php endif; ?>

<?php $registered = $this->Session->flash('registered'); ?>
<?php if(!empty($registered)): ?>
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<strong>Success!</strong> User registration completed successfully.
			</div>
<?php endif; ?>

			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">Change Logs (Most recent <?php echo count($change_logs); ?> records are listed.)</h3>
				</div>
				<div class="panel-body">
<?php foreach($change_logs as $change_log): ?>
					<p>
						<?php 
							echo $change_log['ChangeLog']['date']."&nbsp&nbsp-&nbsp&nbsp";
							switch($change_log['ChangeLog']['content_type_id']){
								// Articleの編集
								case array_search('article', $contentTypeList):
									echo $languageList[ $change_log['Article']['lang_id'] ].'/'.$controllerNameList[ $change_log['Article']['ViewName']['cont_id'] ].'/'.$viewNameList[ $change_log['Article']['view_id'] ].'/'.$placeList[ $change_log['Article']['place_id'] ].'/Num.'.$change_log['Article']['num'];
									break;

								// Navigation Menuの編集
								case array_search('navigation_menu', $contentTypeList):
									echo $languageList[ $change_log['NavigationMenu']['lang_id'] ].'/'.'NavigationMenu'.'/'.$navigationMenuItemList[ $change_log['NavigationMenu']['menu_item_id'] ];
									break;

								// Commonの編集
								case array_search('common', $contentTypeList):
									echo $languageList[ $change_log['Common']['lang_id'] ].'/'.'Common'.'/'.$commonItemList[ $change_log['Common']['item_id'] ];
									break;
							}
							echo ' by '.$change_log['User']['name'];
						 ?>
					</p>
<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div>
</div>
