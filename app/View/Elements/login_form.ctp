
<ul class="col-md-4 nav nav-pills nav-stacked affix">
	<div class="col col-md-12">
		<?php echo $this->Form->create('User', array(
			'url' => '/users/login',
		  'inputDefaults' => array(
		    'div' => 'form-group',
		    'label' => array(
		    	'class' => 'col col-md-4 control-label'
		    ),
		    'wrapInput' => 'col col-md-8',
		    'class' => 'form-control'
		  ),
		  'class' => array('well', 'form-horizontal'),
		)); ?>

		<fieldset>
		  <legend>Log In</legend>
		  <?php $log_in_status = $this->Session->flash('log_in_status'); ?>
		  <?php $log_in_name = $this->Session->flash('log_in_name'); ?>

		  <?php echo $this->Form->input('log_in_status', array(
		    'id' => 'log_in_status',
		    'type' => 'hidden',
		    'value' => !empty($log_in_status) ? 1 : 0
		  )); ?>
		  <?php echo $this->Form->input('name', array(
		    'type' => 'text',
		    'label' => 'Account Name',
		    'value' => !empty($log_in_name) ? h($log_in_name) : '',
		    'placeholder' => 'Account Name'
		  )); ?>
		  <?php echo $this->Form->input('password', array(
		    'type' => 'password',
		    'label' => 'Password',
		    'placeholder' => 'Password'
		  )); ?>

		  <hr>

		  <?php echo $this->Form->submit('Log In', array(
		    'div' => false,
		    'class' => 'col col-md-offset-9 col-md-3 btn btn-primary',
		  	'before' => $this->Session->flash('log_in_message')
		  )); ?>

		</fieldset>
		<?php echo $this->Form->end(); ?>
	</div>
</ul>
