
<!--<ul class="col-md-4 nav nav-pills nav-stacked affix">-->

<?php echo $this->Form->create('Article', array(
	'id' => 'localization_tree_form',
	'name' => 'localization_tree_form',
	'url' => false,
  'inputDefaults' => array(
    'div' => 'form-group',
    'label' => array(
    	'class' => 'col col-md-3 control-label'
    ),
    'class' => 'form-control'
  ),
  'class' => 'form-horizontal'
)); ?>

<fieldset>

	<?php echo $this->Form->input('selected_content_type', array(
		'type' => 'hidden',
		'id' => 'selected_content_type',
		'value' => -1
	)); ?> 
	<?php echo $this->Form->input('selected_lang_id', array(
		'type' => 'hidden',
		'id' => 'selected_lang_id',
		'value' => -1
	)); ?> 
	<?php echo $this->Form->input('selected_cont_name_id', array(
		'type' => 'hidden',
		'id' => 'selected_cont_name_id',
		'value' => -1
	)); ?> 
	<?php echo $this->Form->input('selected_view_name_id', array(
		'type' => 'hidden',
		'id' => 'selected_view_name_id',
		'value' => -1
	)); ?> 
	<table class="table tree">
		<thead>
	    <tr>
	      <th>Language/Page</th>
	      <th>Last Modified</th>
	    </tr>
	  </thead>
	  <tbody>
<?php $total = 0; ?>
<?php foreach($tree as $lang_idx => $lang): ?>
			<?php echo '<tr class="treegrid-'.(++$total).'">'."\n"; ?>
				<td><?php echo $lang['lang_name']; ?></td><td><?php echo $lang['modified_date']; ?></td>
			</tr>
<?php $parent_lang = $total; ?>
			<?php echo '<tr class="treegrid-'.(++$total).' treegrid-parent-'.($parent_lang).'">'."\n"; ?>
				<td>
					<?php echo '<a target="content" href="" onclick="on_click_navigation_menu('.$lang['lang_id'].');return false;">'.$lang['navigation_menu']['nav_name'].'</a>'; ?>
				</td>
				<td>
					<?php echo $lang['navigation_menu']['modified_date']; ?>
				</td>
			</tr>
			<?php echo '<tr class="treegrid-'.(++$total).' treegrid-parent-'.($parent_lang).'">'."\n"; ?>
				<td>
					<?php echo '<a target="content" href="" onclick="on_click_common('.$lang['lang_id'].');return false;">'.$lang['common']['common_name'].'</a>'; ?>
				</td>
				<td>
					<?php echo $lang['common']['modified_date']; ?>
				</td>
			</tr>
<?php foreach($lang['controllers'] as $cont_name_idx => $cont_name): ?>
			<?php echo '<tr class="treegrid-'.(++$total).' treegrid-parent-'.($parent_lang).'">'."\n"; ?>
				<td><?php echo $cont_name['cont_name']; ?></td><td><?php echo $cont_name['modified_date']; ?></td>
			</tr>
<?php $parent_cont = $total; ?>
<?php foreach($cont_name['views'] as $view_name_idx => $view_name): ?>
			<?php echo '<tr class="treegrid-'.(++$total).' treegrid-parent-'.($parent_cont).'">'."\n"; ?>
				<td>
					<?php echo '<a target="content" href="" onclick="on_click_view_name('.$lang['lang_id'].', '.$cont_name['cont_id'].', '.$view_name['view_id'].');return false;">'.$view_name['view_name'].'</a>'; ?>
				</td>
				<td>
					<?php echo $view_name['modified_date']; ?>
				</td>
			</tr>
<?php endforeach; ?>
<?php endforeach; ?>
<?php endforeach; ?>
		</tbody>
	</table>
</fieldset>
<?php echo $this->Form->end(); ?>
<!--</ul>-->


<?php if($isAdmin == true): ?>
	<?php echo $this->Form->create('Article', array(
		'id' => 'localization_admin_menu',
		'name' => 'localization_admin_menu',
		'url' => '/admins/admin_operation',
	  'inputDefaults' => array(
	    'div' => 'form-group',
	    'label' => array(
	    	'class' => 'col col-md-3 control-label'
	    ),
	    //'wrapInput' => 'col col-md-9',
	    'class' => 'form-control'
	  ),
	  //'class' => 'well'
	  'class' => 'form-horizontal'
	)); ?>

	<fieldset>

		<hr>

		<div class="col col-md-12" style="padding-top:20px;">

			<?php /* Admin用データベース変更パネル このパネルを使用する場合、selected_content_typeは-1となる */ ?>

			<div class="panel panel-danger">
			  <div class="panel-heading">Add New Language</div>
			  <div class="panel-body">
					<p>Input new language name with its language.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('add_lang_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input new language name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Add Language', array(
					  	'id' => 'submit_add_lang',
					  	'name' => 'submit_add_lang',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>

			<div class="panel panel-danger">
			  <div class="panel-heading">Add New Controller</div>
			  <div class="panel-body">
					<p>Input new controller name.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('add_cont_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input new controller name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Add Controller', array(
					  	'id' => 'submit_add_cont_name',
					  	'name' => 'submit_add_cont_name',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>

			<div class="panel panel-danger">
			  <div class="panel-heading">Add New Page</div>
			  <div class="panel-body">
					<p>Input controller name and new page name.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('add_cont_name_of_view', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the controller name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					</div>

					<div class="col col-md-7">
						<?php echo $this->Form->input('add_view_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input new page name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Add Page', array(
					  	'id' => 'submit_add_view_name',
					  	'name' => 'submit_add_view_name',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>

			<div class="panel panel-danger">
			  <div class="panel-heading">Add New Place</div>
			  <div class="panel-body">
					<p>Input new place name.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('add_place_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input new place name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Add Place', array(
					  	'id' => 'submit_add_place',
					  	'name' => 'submit_add_place',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>

			<hr>

			<div class="panel panel-warning">
			  <div class="panel-heading">Change Language Name</div>
			  <div class="panel-body">
					<p>Input the old and new language name.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('old_lang_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the old language name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					</div>

					<div class="col col-md-7">
						<?php echo $this->Form->input('new_lang_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the new language name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Change Name', array(
					  	'id' => 'submit_change_lang',
					  	'name' => 'submit_change_lang',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>

			<div class="panel panel-warning">
			  <div class="panel-heading">Change Controller Name</div>
			  <div class="panel-body">
					<p>Input the old and new controller name.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('old_cont_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the old controller name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					</div>

					<div class="col col-md-7">
						<?php echo $this->Form->input('new_cont_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the new controller name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Change Name', array(
					  	'id' => 'submit_change_cont_name',
					  	'name' => 'submit_change_cont_name',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>

			<div class="panel panel-warning">
			  <div class="panel-heading">Change Page Name</div>
			  <div class="panel-body">
					<p>Input old and new controller/page name.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('old_cont_name_of_view', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the old controller name of the page here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					</div>

					<div class="col col-md-7">
						<?php echo $this->Form->input('old_view_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the old page name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					</div>

					<div class="col col-md-7">
						<?php echo $this->Form->input('new_cont_name_of_view', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the new controller name of the page here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					</div>

					<div class="col col-md-7">
						<?php echo $this->Form->input('new_view_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input new page name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Change Name', array(
					  	'id' => 'submit_change_view_name',
					  	'name' => 'submit_change_view_name',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>

			<div class="panel panel-warning">
			  <div class="panel-heading">Change Place Name</div>
			  <div class="panel-body">
					<p>Input the old and new place name.</p>
					<div class="col col-md-7">
						<?php echo $this->Form->input('old_place_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the old place name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					</div>

					<div class="col col-md-7">
						<?php echo $this->Form->input('new_place_name', array(
							'type' => 'text',
							'label' => false,
				      'placeholder' => 'Input the new place name here...',
						)); ?> 
					</div>
					<div class="col col-md-5">
					  <?php echo $this->Form->submit('Change Name', array(
					  	'id' => 'submit_change_place',
					  	'name' => 'submit_change_place',
					    'div' => false,
					    'class' => 'btn btn-primary col-md-12',
					    'value' => 1,
					  )); ?>
					</div>
			  </div>
			</div>
		</div>
	</fieldset>

<?php echo $this->Form->end(); ?>

<?php endif; ?>
