<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('jquery.treegrid');
		echo $this->Html->css('glytoucan_localization');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>

	<!-- nav bar -->
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Navigation Bar</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">GlyTouCan</a>
	    </div>
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="active"><?php echo $this->Html->link('Localization', array('controller' => 'localizations', 'action' => 'index')); ?></li>
	      </ul>
      <ul class="nav navbar-nav navbar-right">
<?php if($auth->loggedIn()): ?>
<?php if($auth->user()['is_admin'] == 1): ?>
        <li>
          <button id="registrationBtn" type="button" class="btn btn-info flat-navbar-btn" style="margin-top:9px; margin-right:5px;" data-container="body" data-toggle="button" data-placement="bottom">User Registration</button>
        </li>
<?php endif;?>

        <li>
          <button id="changePasswordBtn" type="button" class="btn btn-primary flat-navbar-btn" style="margin-top:9px; margin-right:5px;" data-container="body" data-toggle="button" data-placement="bottom">Change Password</button>
        </li>
        <li>
          <button id="logOutBtn" type="button" class="btn btn-danger flat-navbar-btn" style="margin-top:9px; margin-right:5px;" data-container="body" data-toggle="button" data-placement="bottom">Log Out</button>
        </li>
<?php endif; ?>

      </div>
	  </div>
	</nav>

<?php if($auth->loggedIn()): ?>
<?php if($auth->user()['is_admin'] == 1): ?>
  <div id="registration_container" style="display: none">
    <?php echo $this->Form->create('User', array(
    	'url' => '/users/registration',
      'inputDefaults' => array(
        'div' => 'form-group',
        'label' => array(
        	'class' => 'col col-md-4 control-label'
        ),
        'wrapInput' => 'col col-md-8',
        'class' => 'form-control'
      ),
      'class' => array('form-horizontal'),
    )); ?>
    <fieldset>
      <legend>User Registration</legend>
      <?php $registration_status = $this->Session->flash('registration_status'); ?>
      <?php $privileges = array(
      	0 => 'General User',
      	1 => 'Administrator',
      ); ?>
      <?php echo $this->Form->input('registration_status', array(
        'id' => 'registration_status',
        'type' => 'hidden',
        'value' => !empty($registration_status) ? 1 : 0
      )); ?>
      <?php echo $this->Form->input('name', array(
        'type' => 'text',
        'label' => 'Account Name',
        'placeholder' => 'Account Name'
      )); ?>
      <?php echo $this->Form->input('password', array(
        'type' => 'password',
        'label' => 'Password',
        'placeholder' => 'Password'
      )); ?>
	    <?php echo $this->Form->input('is_admin', array(
	    	'type' => 'select',
	    	'options' => $privileges,
	      'label' => array(
		      'text' => 'Privilege',
	      ),
	    )); ?>
      <hr>
      <?php echo $this->Form->submit('Submit', array(
        'div' => false,
        'class' => 'col col-md-offset-4 col-md-4 btn btn-primary',
        'before' => $this->Session->flash('registration_message')
      )); ?>
      <?php echo $this->Form->submit('Reset', array(
        'div' => false,
        'type' => 'reset',
        'class' => 'col col-md-4 btn btn-danger',
      )); ?>
    </fieldset>
    <?php echo $this->Form->end(); ?>
  </div>
<?php endif;?>

  <div id="change_password_container" style="display: none">
    <?php echo $this->Form->create('User', array(
      //'controller' => 'localizations',
      //'action' => 'change_password',
      'url' => '/users/change_password',
      'inputDefaults' => array(
        'div' => 'form-group',
        'label' => false,
        'wrapInput' => 'col col-md-12',
        'class' => 'form-control'
      ),
      'class' => 'form-horizontal'
    )); ?>
      <fieldset>
        <legend>Change Password Form</legend>
        <p>Input your old password and new password.</p>
        <?php $change_password_status = $this->Session->flash('change_password_status'); ?>
        <?php echo $this->Form->input('change_password_status', array(
          'id' => 'change_password_status',
          'type' => 'hidden',
          'value' => !empty($change_password_status) ? 1 : 0
        )); ?>
        <?php echo $this->Form->input('old_password', array(
          'type' => 'password',
          'placeholder' => 'Old Password'
        )); ?>
        <?php echo $this->Form->input('new_password', array(
          'type' => 'password',
          'placeholder' => 'New Password'
        )); ?>
        <hr>
        <?php echo $this->Form->submit('Submit', array(
          'div' => false,
          'class' => 'col col-md-12 btn btn-primary',
          //'class' => 'col col-md-6 btn btn-primary',
          'before' => $this->Session->flash('change_password_message')
        )); ?>
      </fieldset>
    <?php echo $this->Form->end(); ?>
  </div>

  <div id="logout_container" style="display: none">
    <?php echo $this->Form->create('User', array(
      //'controller' => 'localizations',
      //'action' => 'logout',
      'url' => '/users/logout',
      'inputDefaults' => array(
        'div' => 'form-group',
        'label' => false,
        'wrapInput' => 'col col-md-12',
        'class' => 'form-control'
      ),
      'class' => 'form-horizontal'
    )); ?>
      <fieldset>
        <legend>Log Out</legend>
        <p><strong>Are you sure to log out ?</strong></p>
        <?php $log_out_status = $this->Session->flash('log_out_status'); ?>
        <?php echo $this->Form->input('log_out_status', array(
          'id' => 'log_out_status',
          'type' => 'hidden',
          'value' => !empty($log_out_status) ? 1 : 0
        )); ?>
        <?php echo $this->Form->submit('Log Out', array(
          'class' => 'col col-md-12 btn btn-danger',
          'before' => !empty($log_out_status) ? $this->Session->flash('log_out_message') : ''
        )); ?>
      </fieldset>
    <?php echo $this->Form->end(); ?>
  </div>
<?php endif; ?>

	<div id="container" style="padding-top:60px;">
		<div id="header">
		</div>
		<div id="content">
			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
		</div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js'); ?>
  <script type="text/javascript">
    <?php echo "(window.jQuery || document.write('".( preg_replace('/<\/script>/', '<\/script>', $this->html->script('jquery/1.11.1/jquery.min')) )."'));"; ?>
  </script>

  <?php echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js'); ?>
  <script type="text/javascript">
    <?php echo "(window.jQuery.ui || document.write('".( preg_replace('/<\/script>/', '<\/script>', $this->html->script('jqueryui/1.10.4/jquery-ui.min')) )."'));"; ?>
  </script>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <?php echo $this->Html->script('bootstrap.min'); ?>
  <?php echo $this->Html->script('jquery.treegrid'); ?>
  <?php echo $this->Html->script('jquery.treegrid.bootstrap3'); ?>
  <?php echo $this->Html->script('glytoucan_localization'); ?>
</body>
</html>
