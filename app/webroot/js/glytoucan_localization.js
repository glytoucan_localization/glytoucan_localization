$(function(){
	// 各言語のコントローラ・ビューの一覧表示用ツリーを有効化する
	$('.tree').treegrid({
		initialState: 'collapsed',
		//saveState: true,
		//saveStateMethod: 'cookie'
	});

	// LogOut process
	$('#logOutBtn').popover({ 
		html : true,
		trigger: "click",
		container: 'body',
		content: function() {
			return $('#logout_container').html();
		}
	});

	$('#changePasswordBtn').popover({
		html : true,
		trigger: "click",
		container: 'body',
		content: function(){
			return $('#change_password_container').html();
		}
	});
	$('#registrationBtn').popover({
		html : true,
		trigger: "click",
		container: 'body',
		content: function(){
			return $('#registration_container').html();
		}
	});

	// アコーディオンの開閉ボタン
	var $expand_btn_status = 0;
	$('#expand_btn').on('click', function(event) {
		event.preventDefault();
		if($expand_btn_status == 0){
			$('.panel-collapse').collapse('show');
		}else{
			$('.panel-collapse').collapse('hide');
		}
		$expand_btn_status = ($expand_btn_status == 0) ? 1 : 0;
	});

	$(function(){
		if($('#change_password_status').val() == 1){
			$('#changePasswordBtn').popover('show');
		}
		if($('#registration_status').val() == 1){
			$('#registrationBtn').popover('show');
		}
		if($('#log_out_status').val() == 1){
			$('#logOutBtn').popover('show');
		}
	});

});

// いずれかの共有アイテムを選択した時に呼び出されるメソッド
function on_click_common(lang_id){
	$('#selected_lang_id').val(lang_id);
	var action_target = get_action_target(location.pathname);
	$('#localization_tree_form').attr("action", action_target + "common");
	document.localization_tree_form.submit();
}

// いずれかのナビゲーション メニューを選択した時に呼び出されるメソッド
function on_click_navigation_menu(lang_id){
	$('#selected_lang_id').val(lang_id);
	var action_target = get_action_target(location.pathname);
	$('#localization_tree_form').attr("action", action_target + "navigation_menu");
	document.localization_tree_form.submit();
}

// いずれかのビューを選択した時に呼び出されるメソッド
function on_click_view_name(lang_id, cont_name_id, view_name_id){
	$('#selected_lang_id').val(lang_id);
	$('#selected_cont_name_id').val(cont_name_id);
	$('#selected_view_name_id').val(view_name_id);
	var action_target = get_action_target(location.pathname);
	$('#localization_tree_form').attr("action", action_target + "article");
	document.localization_tree_form.submit();
}

function get_action_target(current_path){
	var elements = current_path.split('/');
	var str = "";
	for(var i=0;i<elements.length;i++){
		str += elements[i];
		str += "/";
		if(elements[i].indexOf("localizations") != -1){
			return str;
		}
	}
	return "";
}
