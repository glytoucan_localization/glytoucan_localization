-- MySQL dump 10.13  Distrib 5.5.34, for osx10.6 (i386)
--
-- Host: localhost    Database: glytoucan_localization
-- ------------------------------------------------------
-- Server version	5.5.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

create database glytoucan_localization;
use glytoucan_localization;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `view_id` int(10) unsigned NOT NULL,
  `place_id` int(10) unsigned NOT NULL,
  `num` int(10) unsigned NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `article` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_art_1` (`user_id`),
  KEY `fk_art_2` (`lang_id`),
  KEY `fk_art_3` (`view_id`),
  KEY `fk_art_4` (`place_id`),
  CONSTRAINT `fk_art_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_art_2` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_art_3` FOREIGN KEY (`view_id`) REFERENCES `view_names` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_art_4` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (1,1,1,27,5,1,'2014-11-06 08:50:03','2014-11-06 08:50:03','GlyTouCan is the web interface for the international glycan structure repository.  This repository is a freely available, uncurated registry for glycan structures that assigns globally unique accession numbers to any glycan independent of the level of information provided by the experimental method used to identify the structure(s). Any glycan structure, ranging in resolution from monosaccharide composition to fully defined structures including glycosidic linkage configuration, can be registered as long as there are no inconsistencies in the structure.\r\n\r\nUsers can search for glycan structures and motifs that have been registered into this repository.  Registered users can additionally register new glycan structures to obtain unique IDs for each structure, which can be used in publications and other databases upon approval.\r\n\r\nThe development of this repository is funded by the Integrated Database Project by MEXT (Ministry of Education, Culture, Sports, Science & Technology) and the Program for Coordination Toward Integration of Related Databases by JST (Japan Science and Technology Agency).'),(2,1,1,27,13,1,'2014-11-06 08:50:03','2014-11-06 08:50:03','Copyright © 2014 GlyTouCan'),(3,1,1,16,1,1,'2014-11-06 08:52:31','2014-11-06 08:52:31','Glycan Registration'),(4,1,1,16,4,1,'2014-11-06 08:52:31','2014-11-06 08:52:31','Example of GlycoCT condensed format:\r\n                               The glycan containing repeating units in GlycoCT format.'),(5,1,1,16,5,1,'2014-11-06 08:52:31','2014-11-06 08:52:31','\r\n  RES\r\n  1b:a-dgal-HEX-1:5\r\n  2s:N-acetyl\r\n  3b:b-dgal-HEX-1:5\r\n  4r:r1\r\n  5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\r\n  6s:n-acetyl\r\n  7r:r2\r\n  8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\r\n  9s:n-acetyl\r\n  LIN\r\n  1:1d(2+1)2n\r\n  2:1o(3+1)3d\r\n  3:3o(3+1)4n\r\n  4:4n(3+2)5d\r\n  5:5d(5+1)6n\r\n  6:1o(6+1)7n\r\n  7:7n(3+2)8d\r\n  8:8d(5+1)9n\r\n  REP\r\n  REP1:13o(3+1)10d=-1--1\r\n  RES\r\n  10b:b-dglc-HEX-1:5\r\n  11s:n-acetyl\r\n  12b:a-lgal-HEX-1:5|6:d\r\n  13b:b-dgal-HEX-1:5\r\n  14s:sulfate\r\n  LIN\r\n  9:10d(2+1)11n\r\n  10:10o(3+1)12d\r\n  11:10o(4+1)13d\r\n  12:10o(6+1)14n\r\n  REP2:18o(3+1)15d=-1--1\r\n  RES\r\n  15b:b-dgal-HEX-1:5\r\n  16s:n-acetyl\r\n  17b:a-lgal-HEX-1:5|6:d\r\n  18b:b-dgal-HEX-1:5\r\n  19s:sulfate\r\n  LIN\r\n  13:15d(2+1)16n\r\n  14:15o(3+1)17d\r\n  15:15o(4+1)18d\r\n  16:15o(6+1)19n'),(6,1,1,16,7,1,'2014-11-06 08:52:31','2014-11-06 08:52:31','Input your glycan structure(s) below in GlycoCT condensed format.'),(7,1,1,13,1,1,'2014-11-06 08:54:49','2014-11-06 08:54:49','Registration Confirmation'),(8,1,1,13,14,1,'2014-11-06 08:54:49','2014-11-06 08:54:49','The following structure(s) will be registered upon clicking the submit button.'),(9,1,1,13,15,1,'2014-11-06 08:54:49','2014-11-06 08:54:49','An error occurred. Please check sructure(s) again.'),(10,1,1,13,16,1,'2014-11-06 08:54:49','2014-11-06 08:54:49','Structure(s) cannot be parsed. Please check structure(s) and try again.'),(11,1,1,13,17,1,'2014-11-06 08:54:49','2014-11-06 08:54:49','The following structure(s) have already been registered.'),(12,1,1,12,1,1,'2014-11-06 08:55:35','2014-11-06 08:55:35','Complete'),(13,1,1,48,1,1,'2014-11-06 08:56:42','2014-11-06 08:56:42','Structure Search'),(14,1,1,48,7,1,'2014-11-06 08:56:42','2014-11-06 08:56:42',' '),(15,1,1,48,7,2,'2014-11-06 08:57:01','2014-11-06 08:57:01','Search type'),(16,1,1,48,7,3,'2014-11-06 08:57:14','2014-11-06 08:57:14','Search for exact same structure'),(17,1,1,48,7,4,'2014-11-06 08:57:31','2014-11-06 08:57:31','Search for substructure'),(18,1,1,48,7,5,'2014-11-06 08:57:47','2014-11-06 08:57:47','Select sequence format :'),(19,1,1,48,7,6,'2014-11-06 08:58:05','2014-11-06 08:58:05','GlycoCT condensed'),(20,1,1,48,7,7,'2014-11-06 08:58:21','2014-11-06 08:58:21','CarbBank'),(21,1,1,48,7,8,'2014-11-06 08:58:36','2014-11-06 08:58:36','GlycoMinds (Linear Code®)'),(22,1,1,48,7,9,'2014-11-06 08:58:49','2014-11-06 08:58:49','BCSDB'),(23,1,1,48,7,10,'2014-11-06 08:59:01','2014-11-06 08:59:01','LINUCS'),(24,1,1,48,7,11,'2014-11-06 08:59:16','2014-11-06 08:59:16','KCF'),(25,1,1,48,4,1,'2014-11-06 08:59:48','2014-11-06 08:59:48','Search type'),(26,1,1,48,4,2,'2014-11-06 09:00:08','2014-11-06 09:00:08','Search for exact same structure'),(27,1,1,48,4,3,'2014-11-06 09:00:23','2014-11-06 09:00:23','Search for substructure'),(28,1,1,48,5,1,'2014-11-06 09:00:40','2014-11-06 09:00:40','Search for exact same structure with entered glycan structure.'),(29,1,1,48,5,1,'2014-11-06 09:00:40','2014-11-06 09:01:57',' '),(30,1,1,48,5,2,'2014-11-06 09:01:57','2014-11-06 09:01:57','Search for exact same structure with entered glycan structure.'),(31,1,1,48,5,3,'2014-11-06 09:02:30','2014-11-06 09:02:30','Search for all substructures that contain entered glycan structure.'),(32,1,1,48,8,1,'2014-11-06 09:02:50','2014-11-06 09:02:50',' '),(33,1,1,48,8,2,'2014-11-06 09:03:09','2014-11-06 09:03:09','GlycoCT condensed'),(34,1,1,48,9,1,'2014-11-06 09:04:56','2014-11-06 09:04:56',' '),(35,1,1,48,9,2,'2014-11-06 09:05:06','2014-11-06 09:05:06','GlycoCT format is encoding schema for carbohydrate sequences based on a connection table approach to describe carbohydrate sequences.  The format is adopting IUPAC rules to generate a consistent, machine-readable nomenclature using a block concept to describe carbohydrate sequences like repeating units. It consists of two variants, a condensed format and an XML format. The condensed format allows for unique identification of glycan structures in a compact manner.\r\nThe monosaccharide naming convention follows the following format: a-bcc-DDD-e : f|g : h, where a is the anomeric configuration (one of a, b, o, x), b is the stereoisomer configuration (one of d, 1, x), ccc is the three-letter code for the monosaccharide as listed in Table 1.1, DDD is the base type or superclass indicating the number of consecutive carbon atoms such as HEX, PEN,  NON, e and f indicate the carbon numbers involved in closing the ring, g is the position of the modifier, and h is the type of modifier. For a, b, e, f and g, an x can be used to specify an unknown value. bcc and g : h may also be repeated if necessary. \r\nIt is noted that substituents of monosaccharides are also treated as separate residues attached to the base residue. These substituents are distinguished by specifying one of the base residue. These substituents are distinguished by specifying one of the following codes immediately after the residue number: b=basetype, s=substituent, r=repeating unit, a=alternative unit. The list of substituents handled by GlycoCT is given in Table 1.2.\r\nThe GlycoCT format follows something similar to the KCF format, where the residues are specified in a RES section, and the linkage in a LIN section.'),(36,1,1,48,10,1,'2014-11-06 09:05:42','2014-11-06 09:05:42',' '),(37,1,1,48,10,2,'2014-11-06 09:05:52','2014-11-06 09:05:52','<br>\r\nTABLE 1.1: List of monosaccharide and their three-letter codes used in GlycoCT.\r\n<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\">\r\n  <tr style=\"background:#ccccff\">\r\n    <th>Monosaccharide name</th>\r\n    <th>Three-letter code</th>\r\n    <th>Superclass</th>\r\n  </tr>\r\n  <tr>\r\n    <td>Allose</td>\r\n    <td>ALL</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Altrose</td>\r\n    <td>ALT</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Arabinose</td>\r\n    <td>ARA</td>\r\n    <td>PEN</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Erythrose</td>\r\n    <td>ERY</td>\r\n    <td>TET</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Galactose</td>\r\n    <td>GAL</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Glucose</td>\r\n    <td>GLC</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Glyceraldehyde</td>\r\n    <td>GRO</td>\r\n    <td>TRI</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Gulose</td>\r\n    <td>GUL</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Idose</td>\r\n    <td>IDO</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Lyxose</td>\r\n    <td>LYX</td>\r\n    <td>PEN</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Mannose</td>\r\n    <td>MAN</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Ribose</td>\r\n    <td>RIB</td>\r\n    <td>PEN</td>\r\n  </tr>\r\n   <tr>\r\n    <td>Talose</td>\r\n    <td>TAL</td>\r\n    <td>HEX</td>\r\n  </tr>\r\n   <tr>\r\n    <td>Threose</td>\r\n    <td>TRE</td>\r\n    <td>TET</td>\r\n  </tr>\r\n   <tr>\r\n    <td>Xylose</td>\r\n    <td>XYL</td>\r\n    <td>PEN</td>\r\n  </tr>\r\n</table>\r\n<br>\r\nTABLE 1.2: List of substituents used in GlycoCT.\r\n<table border=\"1\" cellspacing=\"0\" cellpadding=\"3\">\r\n  <tr>\r\n    <td>acetyl</td>\r\n    <td>amidino</td>\r\n    <td>amino</td>\r\n  </tr>\r\n  <tr>\r\n    <td>anhydro</td>\r\n    <td>bromo</td>\r\n    <td>chloro</td>\r\n  </tr>\r\n  <tr>\r\n    <td>diphospho</td>\r\n    <td>epoxy</td>\r\n    <td>ethanolamine</td>\r\n  </tr>\r\n  <tr>\r\n    <td>ethyl</td>\r\n    <td>fluoro</td>\r\n    <td>formyl</td>\r\n  </tr>\r\n  <tr>\r\n    <td>glycolyl</td>\r\n    <td>hydroxymethyl</td>\r\n    <td>imino</td>\r\n  </tr>\r\n  <tr>\r\n    <td>iodo</td>\r\n    <td>lactone</td>\r\n    <td>methyl</td>\r\n  </tr>\r\n  <tr>\r\n    <td>N-acetyl</td>\r\n    <td>N-alanine</td>\r\n    <td>N-amidino</td>\r\n  </tr>\r\n  <tr>\r\n    <td>N-dimethyl</td>\r\n    <td>N-formyl</td>\r\n    <td>N-glycolyl</td>\r\n  </tr>\r\n  <tr>\r\n    <td>N-methyl</td>\r\n    <td>N-methyl-carbomoyl</td>\r\n    <td>N-succinate</td>\r\n  </tr>\r\n  <tr>\r\n    <td>N-sulfate</td>\r\n    <td>N-triflouroacetyl</td>\r\n    <td>nitrate</td>\r\n  </tr>\r\n  <tr>\r\n    <td>phosphate</td>\r\n    <td>phospho-choline</td>\r\n    <td>phospho-ethanolamine</td>\r\n  </tr>\r\n  <tr>\r\n    <td>pyrophosphate</td>\r\n    <td>pyruvate</td>\r\n    <td>succinate</td>\r\n  </tr>\r\n  <tr>\r\n    <td>sulfate</td>\r\n    <td>thio</td>\r\n    <td>triphosphate</td>\r\n  </tr>\r\n  <tr>\r\n    <td>(r)-1-hydroxyethyl</td>\r\n    <td>(r)-carboxyethyl</td>\r\n    <td>(r)-carboxymethyl</td>\r\n  </tr>\r\n  <tr>\r\n    <td>(r)-lactate</td>\r\n    <td>(r)-pyruvate</td>\r\n    <td>(s)-1-hydroxyethyl</td>\r\n  </tr>\r\n  <tr>\r\n    <td>(s)-carboxyethyl</td>\r\n    <td>(s)-carboxymethyl</td>\r\n    <td>(s)-lactate</td>\r\n  </tr>\r\n  <tr>\r\n    <td>(s)-pyruvate</td>\r\n    <td>(x)-lactate</td>\r\n    <td>(x)-pyruvate</td>\r\n  </tr>\r\n</table>\r\n<br>\r\nExample of GlycoCT: The glycan containing repeating units in GlycoCT format.\r\n<pre>\r\n  RES\r\n  1b:a-dgal-HEX-1:5\r\n  2s:n-acetyl\r\n  3b:b-dgal-HEX-1:5\r\n  4r:r1\r\n  5b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\r\n  6s:n-acetyl\r\n  7r:r2\r\n  8b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\r\n  9s:n-acetyl\r\n  LIN\r\n  1:1d(2+1)2n\r\n  2:1o(3+1)3d\r\n  3:3o(3+1)4n\r\n  4:4n(3+2)5d\r\n  5:5d(5+1)6n\r\n  6:1o(6+1)7n\r\n  7:7n(3+2)8d\r\n  8:8d(5+1)9n\r\n  REP\r\n  REP1:13o(3+1)10d=-1--1\r\n  RES\r\n  10b:b-dglc-HEX-1:5\r\n  11s:n-acetyl\r\n  12b:a-lgal-HEX-1:5|6:d\r\n  13b:b-dgal-HEX-1:5\r\n  14s:sulfate\r\n  LIN\r\n  9:10d(2+1)11n\r\n  10:10o(3+1)12d\r\n  11:10o(4+1)13d\r\n  12:10o(6+1)14n\r\n  REP2:18o(3+1)15d=-1--1\r\n  RES\r\n  15b:b-dgal-HEX-1:5\r\n  16s:n-acetyl\r\n  17b:a-lgal-HEX-1:5|6:d\r\n  18b:b-dgal-HEX-1:5\r\n  19s:sulfate\r\n  LIN\r\n  13:15d(2+1)16n\r\n  14:15o(3+1)17d\r\n  15:15o(4+1)18d\r\n  16:15o(6+1)19n\r\n</pre>\r\n'),(38,1,1,48,8,3,'2014-11-06 09:07:30','2014-11-06 09:07:30',' '),(39,1,1,48,8,4,'2014-11-06 09:07:40','2014-11-06 09:07:40',' '),(40,1,1,48,8,5,'2014-11-06 09:08:58','2014-11-06 09:08:58','CarbBank'),(41,1,1,48,9,3,'2014-11-06 09:09:21','2014-11-06 09:09:21',' '),(42,1,1,48,9,4,'2014-11-06 09:09:35','2014-11-06 09:09:35',' '),(43,1,1,48,9,5,'2014-11-06 09:09:59','2014-11-06 09:09:59','IUPAC suggests an extended IUPAC form by which structures are written across multiple lines. This  is the format originally used by CarbBank, thus it is sometimes referred to as such. The representation of monosaccharides is the same as that of IUPAC format, where each monosaccharides residue is preceded by the anomeric descriptor and the configuration symbol and the ring size is indicated by an italic f or p. If any of α/β, D/L, f/p are omitted, it is assumed that this structural detail is unknown.\r\n    This format is may substitute α and β with a and b, respectively. Arrows (→) may also be replaced by hyphens (-)、and up (↑) and down (↓) arrows may be replaced by bars (|).'),(44,1,1,48,10,3,'2014-11-06 09:10:27','2014-11-06 09:10:27',' '),(45,1,1,48,10,4,'2014-11-06 09:10:48','2014-11-06 09:10:48',' '),(46,1,1,48,10,5,'2014-11-06 09:11:07','2014-11-06 09:11:07','<pre>\r\nExample of CarbBank format: The N-glycan core structure represented in CarbBank (extended IUPAC) format. \r\n    a-D-Manp-(1-6)+\r\n                  |\r\n             b-D-Manp-(1-4)-b-D-GlcpNAc-(1-4)-a-D-GlcpNAc\r\n                  |\r\n    a-D-Manp-(1-3)+             \r\n</pre>'),(47,1,1,48,8,6,'2014-11-06 09:11:42','2014-11-06 09:11:42','GlycoMinds (Linear Code®)'),(48,1,1,48,9,6,'2014-11-06 09:12:08','2014-11-06 09:12:08','The Carbohydrate format,  GlycoMinds which is also  known as Linear Code®,  uses a single-letter nomenclature for monosaccharides and includes a condensed description of the glycosidic linkages. Monosaccharide representation is based on the common structure of a monosaccharide where modifications to the common structure are indicated by specific symbols, as in the following (Banin el al.(2002)).\r\n Stereoisomers (D or L) differing from the common isomer are indicated by apostrophe (‘).\r\nMonosaccharides with differing ring size (furanose or pyranose) from the common form are indicated by a caret (^).\r\nMonosaccharides differing in both of the above are indicated by a tilde (~).'),(49,1,1,48,10,6,'2014-11-06 09:12:56','2014-11-06 09:12:56','\r\nTABLE 1.3 : List of common modifications as used in the Linear Code&reg format.\r\n<table cellspacing=\"2\" cellpadding=\"3\">\r\n  <tr style=\"background:#eeeeee\">\r\n    <th>Modification Type</th>\r\n    <th>Linear Code&reg</th>\r\n  </tr>\r\n  <tr>\r\n    <td>deacetylated N-acetyl</td>\r\n    <td>Q</td>\r\n  </tr>\r\n  <tr>\r\n    <td>ethanolaminephosphate</td>\r\n    <td>PE</td>\r\n  </tr>\r\n  <tr>\r\n    <td>inositol</td>\r\n    <td>IN</td>\r\n  </tr>\r\n  <tr>\r\n    <td>methyl</td>\r\n    <td>ME</td>\r\n  </tr>\r\n  <tr>\r\n    <td>N-acetyl</td>\r\n    <td>N</td>\r\n  </tr>\r\n  <tr>\r\n    <td>O-acetyl</td>\r\n    <td>T</td>\r\n  </tr>\r\n  <tr>\r\n    <td>phosphate</td>\r\n    <td>P</td>\r\n  </tr>\r\n  <tr>\r\n    <td>phosphocholine</td>\r\n    <td>PC</td>\r\n  </tr>\r\n    <tr>\r\n    <td>pyruvate</td>\r\n    <td>PYR</td>\r\n  </tr>\r\n  <tr>\r\n    <td>sulfate</td>\r\n    <td>S</td>\r\n  </tr>\r\n  <tr>\r\n    <td>sulfide</td>\r\n    <td>SH</td>\r\n  </tr>\r\n  <tr>\r\n    <td>2-aminoethylphosphonic acid</td>\r\n    <td>EP</td>\r\n  </tr>\r\n</table>\r\n\r\nExample of GlycoMinds(Linear Code®):<pre>\r\n   GNb2(Ab4GNb4)Ma3(Ab4GNb2(Fa3(Ab4)GNb6)Ma6)Mb4GNb4GN </pre>'),(50,1,1,48,10,6,'2014-11-06 09:12:56','2014-11-06 09:13:16','TABLE 1.3 : List of common modifications as used in the Linear Code&reg format.\r\n<table cellspacing=\"2\" cellpadding=\"3\">\r\n  <tr style=\"background:#eeeeee\">\r\n    <th>Modification Type</th>\r\n    <th>Linear Code&reg</th>\r\n  </tr>\r\n  <tr>\r\n    <td>deacetylated N-acetyl</td>\r\n    <td>Q</td>\r\n  </tr>\r\n  <tr>\r\n    <td>ethanolaminephosphate</td>\r\n    <td>PE</td>\r\n  </tr>\r\n  <tr>\r\n    <td>inositol</td>\r\n    <td>IN</td>\r\n  </tr>\r\n  <tr>\r\n    <td>methyl</td>\r\n    <td>ME</td>\r\n  </tr>\r\n  <tr>\r\n    <td>N-acetyl</td>\r\n    <td>N</td>\r\n  </tr>\r\n  <tr>\r\n    <td>O-acetyl</td>\r\n    <td>T</td>\r\n  </tr>\r\n  <tr>\r\n    <td>phosphate</td>\r\n    <td>P</td>\r\n  </tr>\r\n  <tr>\r\n    <td>phosphocholine</td>\r\n    <td>PC</td>\r\n  </tr>\r\n    <tr>\r\n    <td>pyruvate</td>\r\n    <td>PYR</td>\r\n  </tr>\r\n  <tr>\r\n    <td>sulfate</td>\r\n    <td>S</td>\r\n  </tr>\r\n  <tr>\r\n    <td>sulfide</td>\r\n    <td>SH</td>\r\n  </tr>\r\n  <tr>\r\n    <td>2-aminoethylphosphonic acid</td>\r\n    <td>EP</td>\r\n  </tr>\r\n</table>\r\n\r\nExample of GlycoMinds(Linear Code®):<pre>\r\n   GNb2(Ab4GNb4)Ma3(Ab4GNb2(Fa3(Ab4)GNb6)Ma6)Mb4GNb4GN </pre>'),(51,1,1,48,8,7,'2014-11-06 09:13:16','2014-11-06 09:13:16','BCSDB'),(52,1,1,48,9,7,'2014-11-06 09:13:40','2014-11-06 09:13:40','The Bacterial Carbohydrate Structure DataBase(BCSDB) format is used in the BCSDB database to encode carbohydrates and derivative structures in a single line.  \r\n Residues are described in the format <res>(<c1>-<c2>) where res is the name of the residue and its configuration and c1 and c2 correspond to the carbon numbers of the child and parent, respectively, by which the residue res is linked to its parent.'),(53,1,1,48,10,7,'2014-11-06 09:14:03','2014-11-06 09:14:03','<br>\r\nExample of BCSDB format: Chemical repeating unit of polymer.<pre>\r\n 		\r\na-D-GlcpA-(1-3)-+          \r\n                |          \r\n-3)-a-D-Glcp-(1-4)-b-D-Manp-(1-4)-b-D-Glcp-(1-\r\n</pre>'),(54,1,1,48,8,8,'2014-11-06 09:14:25','2014-11-06 09:14:25','LINUCS'),(55,1,1,48,9,8,'2014-11-06 09:14:44','2014-11-06 09:14:44','The LInear Notation for Unique description of Carbohydrate Sequences (LINUCS) format is based on the extended IUPAC format but uses additional rules to define the priority of the branches. In this way, carbohydrate structure can be defined uniquely while still containing all the information required to describe the structure.\r\n The start of LINUCS format may include two square brackets [], followed by the root residue name in square brackets. If a residue has a single child, then the child’s linkage in parentheses surrounded by square brackets precedes the child’s residue name and configuration (as in IUPAC format) in square brackets. if a residue has more than one child, then each child’s branch is surrounded by curly brackets {}. Children are listed in order of the carbon number linking them to the parent, such that the child with a 1-3 linkage would come before a child with a 1-4 linkage.'),(56,1,1,48,10,8,'2014-11-06 09:15:09','2014-11-06 09:15:09','<br>\r\n  Example of LINUCS: The glycan structure in LINUCS format.\r\n<pre>\r\n    [][Asn]{\r\n      [(4+1)][b-D-GlcpNAc]{\r\n        [(4+1)][b-D-GlcpNAc]{\r\n          [(4+1)][b-D-Manp]{\r\n            [(3+1)][a-D-Manp]{\r\n              [(2+1)][a-D-Manp]{\r\n                [(2+1)][a-D-Manp]{}\r\n                }\r\n              }\r\n            [(6+1)][a-D-Manp]{\r\n              [(3+1)][a-D-Manp]{\r\n              	[(2+1)][a-D-Manp]{}\r\n                }\r\n              [(6+1)][a-D-Manp]{\r\n                [(2+1)][a-D-Manp]{}\r\n                }\r\n              }\r\n            }\r\n          }\r\n        }\r\n      }  	\r\n</pre>\r\n'),(57,1,1,48,8,9,'2014-11-06 09:16:10','2014-11-06 09:16:10','KCF'),(58,1,1,48,9,9,'2014-11-06 09:16:10','2014-11-06 09:16:10','The KEGG Chemical Function (KCF) format for representing glycan structures was originally used to represent chemical structures (thus the name) in KEGG. KCF uses the graph notation, where nodes are monosaccharides and edges are glycosidic linkages. Thus to represent a glycan, at least three sections are required: ENTRY, NODE, EDGE, followed by three slashes ‘///’ at the end.\r\n'),(59,1,1,48,10,9,'2014-11-06 09:16:10','2014-11-06 09:16:10','\r\n<ul><li>The ENTRY section consists of one line and may specify a name for the structure followed by the keyword Glycan.\r\n<li>The NODE section consists of several lines. The first line contains the number of monosaccharides or aglycon entities, and the following lines consist of the details of these entities numbered consecutively. For each entity line, the name and x- and y-coordinates (to draw on a 2D plane) must be specified.\r\n<li>Similarly, the EDGE section consists of several lines, the first line containing the number of bonds (usually one less than the number of NODEs), followed by the details of the bond information. The format for the bond information is as follows:\r\n<br><br><ul><li>\'.$insert.\'</ul><br>\r\n Example of KCF format: The N-glycan core structure represented in KCF format. \r\n<pre>\r\n  ENTRY     XYZ          Glycan\r\n  NODE      5     \r\n            1     GlcNAc     15.0     7.0\r\n            2     GlcNAc      8.0     7.0\r\n            3     Man         1.0     7.0\r\n            4     Man        -6.0    12.0\r\n            5     Man        -6.0     2.0\r\n  EDGE      4\r\n            1     2:b1       1:4\r\n            2     3:b1       2:4\r\n            3     5:al       3:3\r\n            4     4:al       3:6\r\n  ///\r\n</pre>\r\n'),(60,1,1,44,1,1,'2014-11-06 09:17:21','2014-11-06 09:17:21','Composition Search'),(61,1,1,44,3,1,'2014-11-06 09:17:21','2014-11-06 09:17:21','The composition search finds structures that satisfy the minimum and maximum requirements specified for each component.'),(62,1,1,44,7,1,'2014-11-06 09:17:21','2014-11-06 09:17:21',' '),(63,1,1,4,1,1,'2014-11-06 09:18:09','2014-11-06 09:18:09','Motif List'),(64,1,1,4,3,1,'2014-11-06 09:18:09','2014-11-06 09:18:09','Motifs are common structural patterns that are often found in glycans.\r\nThis is a list of all motifs registered in the repository.\r\nTags refer to labels attached to motifs to group them together.'),(65,1,1,11,1,1,'2014-11-06 09:18:52','2014-11-06 09:18:52','Preferences'),(66,1,1,11,2,1,'2014-11-06 09:18:52','2014-11-06 09:18:52','Change the graphical representation of glycan'),(67,1,1,11,3,1,'2014-11-06 09:18:52','2014-11-06 09:18:52','Select image notation'),(68,1,1,11,2,2,'2014-11-06 09:19:22','2014-11-06 09:19:22','Language'),(69,1,1,11,3,2,'2014-11-06 09:19:22','2014-11-06 09:19:22',' '),(70,1,1,12,3,1,'2014-11-06 09:20:26','2014-11-06 09:20:26',' ');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `change_logs`
--

DROP TABLE IF EXISTS `change_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `change_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(10) unsigned NOT NULL,
  `content_type_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_log_1` (`user_id`),
  KEY `fk_log_2` (`content_type_id`),
  CONSTRAINT `fk_log_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_log_2` FOREIGN KEY (`content_type_id`) REFERENCES `content_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `change_logs`
--

LOCK TABLES `change_logs` WRITE;
/*!40000 ALTER TABLE `change_logs` DISABLE KEYS */;
INSERT INTO `change_logs` VALUES (1,'2014-11-06 08:48:26',1,2,1),(2,'2014-11-06 08:48:26',1,2,2),(3,'2014-11-06 08:48:26',1,2,3),(4,'2014-11-06 08:48:26',1,2,4),(5,'2014-11-06 08:48:26',1,2,5),(6,'2014-11-06 08:48:26',1,2,6),(7,'2014-11-06 08:48:26',1,2,7),(8,'2014-11-06 08:48:26',1,2,8),(9,'2014-11-06 08:48:26',1,2,9),(10,'2014-11-06 08:48:26',1,2,10),(11,'2014-11-06 08:48:26',1,2,11),(12,'2014-11-06 08:48:26',1,2,12),(13,'2014-11-06 08:48:26',1,2,13),(14,'2014-11-06 08:48:26',1,2,14),(15,'2014-11-06 08:48:26',1,2,15),(16,'2014-11-06 08:48:26',1,2,16),(17,'2014-11-06 08:48:26',1,2,17),(18,'2014-11-06 08:48:26',1,2,18),(19,'2014-11-06 08:48:26',1,2,19),(20,'2014-11-06 08:48:26',1,2,20),(21,'2014-11-06 08:48:26',1,2,21),(22,'2014-11-06 08:48:26',1,2,22),(23,'2014-11-06 08:48:26',1,2,23),(24,'2014-11-06 08:48:26',1,2,24),(25,'2014-11-06 08:48:26',1,2,25),(26,'2014-11-06 08:48:26',1,2,26),(27,'2014-11-06 08:48:26',1,2,27),(28,'2014-11-06 08:48:26',1,2,28),(29,'2014-11-06 08:48:26',1,2,29),(30,'2014-11-06 08:48:26',1,2,30),(31,'2014-11-06 08:48:26',1,2,31),(32,'2014-11-06 08:48:26',1,2,32),(33,'2014-11-06 08:50:03',1,1,1),(34,'2014-11-06 08:50:03',1,1,2),(35,'2014-11-06 08:52:31',1,1,3),(36,'2014-11-06 08:52:31',1,1,4),(37,'2014-11-06 08:52:31',1,1,5),(38,'2014-11-06 08:52:31',1,1,6),(39,'2014-11-06 08:54:49',1,1,7),(40,'2014-11-06 08:54:49',1,1,8),(41,'2014-11-06 08:54:49',1,1,9),(42,'2014-11-06 08:54:49',1,1,10),(43,'2014-11-06 08:54:49',1,1,11),(44,'2014-11-06 08:55:35',1,1,12),(45,'2014-11-06 08:56:42',1,1,13),(46,'2014-11-06 08:56:42',1,1,14),(47,'2014-11-06 08:57:01',1,1,15),(48,'2014-11-06 08:57:14',1,1,16),(49,'2014-11-06 08:57:32',1,1,17),(50,'2014-11-06 08:57:47',1,1,18),(51,'2014-11-06 08:58:05',1,1,19),(52,'2014-11-06 08:58:21',1,1,20),(53,'2014-11-06 08:58:36',1,1,21),(54,'2014-11-06 08:58:49',1,1,22),(55,'2014-11-06 08:59:01',1,1,23),(56,'2014-11-06 08:59:16',1,1,24),(57,'2014-11-06 08:59:48',1,1,25),(58,'2014-11-06 09:00:08',1,1,26),(59,'2014-11-06 09:00:23',1,1,27),(60,'2014-11-06 09:00:40',1,1,28),(61,'2014-11-06 09:01:57',1,1,29),(62,'2014-11-06 09:01:57',1,1,30),(63,'2014-11-06 09:02:30',1,1,31),(64,'2014-11-06 09:02:50',1,1,32),(65,'2014-11-06 09:03:09',1,1,33),(66,'2014-11-06 09:04:56',1,1,34),(67,'2014-11-06 09:05:06',1,1,35),(68,'2014-11-06 09:05:42',1,1,36),(69,'2014-11-06 09:05:52',1,1,37),(70,'2014-11-06 09:07:30',1,1,38),(71,'2014-11-06 09:07:40',1,1,39),(72,'2014-11-06 09:08:58',1,1,40),(73,'2014-11-06 09:09:21',1,1,41),(74,'2014-11-06 09:09:35',1,1,42),(75,'2014-11-06 09:09:59',1,1,43),(76,'2014-11-06 09:10:27',1,1,44),(77,'2014-11-06 09:10:48',1,1,45),(78,'2014-11-06 09:11:07',1,1,46),(79,'2014-11-06 09:11:42',1,1,47),(80,'2014-11-06 09:12:08',1,1,48),(81,'2014-11-06 09:12:56',1,1,49),(82,'2014-11-06 09:13:16',1,1,50),(83,'2014-11-06 09:13:16',1,1,51),(84,'2014-11-06 09:13:40',1,1,52),(85,'2014-11-06 09:14:03',1,1,53),(86,'2014-11-06 09:14:25',1,1,54),(87,'2014-11-06 09:14:44',1,1,55),(88,'2014-11-06 09:15:09',1,1,56),(89,'2014-11-06 09:16:10',1,1,57),(90,'2014-11-06 09:16:10',1,1,58),(91,'2014-11-06 09:16:10',1,1,59),(92,'2014-11-06 09:17:21',1,1,60),(93,'2014-11-06 09:17:21',1,1,61),(94,'2014-11-06 09:17:21',1,1,62),(95,'2014-11-06 09:18:09',1,1,63),(96,'2014-11-06 09:18:09',1,1,64),(97,'2014-11-06 09:18:52',1,1,65),(98,'2014-11-06 09:18:52',1,1,66),(99,'2014-11-06 09:18:52',1,1,67),(100,'2014-11-06 09:19:22',1,1,68),(101,'2014-11-06 09:19:22',1,1,69),(102,'2014-11-06 09:20:26',1,1,70);
/*!40000 ALTER TABLE `change_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content_types`
--

DROP TABLE IF EXISTS `content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content_types`
--

LOCK TABLES `content_types` WRITE;
/*!40000 ALTER TABLE `content_types` DISABLE KEYS */;
INSERT INTO `content_types` VALUES (1,'article'),(2,'navigation_menu');
/*!40000 ALTER TABLE `content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controller_names`
--

DROP TABLE IF EXISTS `controller_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controller_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controller_names`
--

LOCK TABLES `controller_names` WRITE;
/*!40000 ALTER TABLE `controller_names` DISABLE KEYS */;
INSERT INTO `controller_names` VALUES (1,'Facets'),(2,'Motifs'),(3,'Preferences'),(4,'Registries'),(5,'Stanzas'),(6,'Structures'),(7,'Users');
/*!40000 ALTER TABLE `controller_names` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English'),(3,'中文(简体)'),(4,'中文(繁體)'),(2,'日本語');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `navigation_menu_items`
--

DROP TABLE IF EXISTS `navigation_menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navigation_menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navigation_menu_items`
--

LOCK TABLES `navigation_menu_items` WRITE;
/*!40000 ALTER TABLE `navigation_menu_items` DISABLE KEYS */;
INSERT INTO `navigation_menu_items` VALUES (7,'navbar_accessionNumberPlaceholder'),(1,'navbar_homeLabel'),(4,'navbar_preferencesLabel'),(8,'navbar_searchButton'),(2,'navbar_searchLabel'),(10,'navbar_searchLabelItem_compositionSearchLabel'),(12,'navbar_searchLabelItem_graphicalSearchLabel'),(11,'navbar_searchLabelItem_motifSearchLabel'),(9,'navbar_searchLabelItem_structureSearchLabel'),(5,'navbar_signInLabel'),(6,'navbar_signUpLabel'),(3,'navbar_viewAllLabel'),(14,'navbar_viewAllLabelItem_glycanListLabel'),(13,'navbar_viewAllLabelItem_motifListLabel'),(18,'signIn_emailPasswordButton'),(17,'signIn_passwordLabel'),(19,'signIn_recoverUserNameButton'),(21,'signIn_socialLoginText'),(20,'signIn_submitButton'),(15,'signIn_titleLabel'),(16,'signIn_userNameLabel'),(30,'signUp_affiliationLabel'),(27,'signUp_confirmPasswordLabel'),(29,'signUp_emailLabel'),(32,'signUp_footerText'),(28,'signUp_fullNameLabel'),(26,'signUp_passwordLabel'),(25,'signUp_passwordText'),(31,'signUp_submitButton'),(22,'signUp_titleLabel'),(24,'signUp_userNameLabel'),(23,'signUp_userNameText');
/*!40000 ALTER TABLE `navigation_menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `navigation_menus`
--

DROP TABLE IF EXISTS `navigation_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `navigation_menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `lang_id` int(10) unsigned NOT NULL,
  `menu_item_id` int(10) unsigned NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `data` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_nav_1` (`user_id`),
  KEY `fk_nav_2` (`lang_id`),
  KEY `fk_nav_3` (`menu_item_id`),
  CONSTRAINT `fk_nav_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_nav_2` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_nav_3` FOREIGN KEY (`menu_item_id`) REFERENCES `navigation_menu_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `navigation_menus`
--

LOCK TABLES `navigation_menus` WRITE;
/*!40000 ALTER TABLE `navigation_menus` DISABLE KEYS */;
INSERT INTO `navigation_menus` VALUES (1,1,1,1,'2014-11-06 08:48:26','2014-11-06 08:48:26','Home'),(2,1,1,2,'2014-11-06 08:48:26','2014-11-06 08:48:26','Search'),(3,1,1,3,'2014-11-06 08:48:26','2014-11-06 08:48:26','View All'),(4,1,1,4,'2014-11-06 08:48:26','2014-11-06 08:48:26','Preferences'),(5,1,1,5,'2014-11-06 08:48:26','2014-11-06 08:48:26','SignIn'),(6,1,1,6,'2014-11-06 08:48:26','2014-11-06 08:48:26','SignUp'),(7,1,1,7,'2014-11-06 08:48:26','2014-11-06 08:48:26','Accession Number'),(8,1,1,8,'2014-11-06 08:48:26','2014-11-06 08:48:26','Search'),(9,1,1,9,'2014-11-06 08:48:26','2014-11-06 08:48:26','Structure Search'),(10,1,1,10,'2014-11-06 08:48:26','2014-11-06 08:48:26','Composition Search'),(11,1,1,11,'2014-11-06 08:48:26','2014-11-06 08:48:26','Motif Search'),(12,1,1,12,'2014-11-06 08:48:26','2014-11-06 08:48:26','Graphical Search'),(13,1,1,13,'2014-11-06 08:48:26','2014-11-06 08:48:26','Motif List'),(14,1,1,14,'2014-11-06 08:48:26','2014-11-06 08:48:26','Glycan List'),(15,1,1,15,'2014-11-06 08:48:26','2014-11-06 08:48:26','SignIn'),(16,1,1,16,'2014-11-06 08:48:26','2014-11-06 08:48:26','Username'),(17,1,1,17,'2014-11-06 08:48:26','2014-11-06 08:48:26','Password'),(18,1,1,18,'2014-11-06 08:48:26','2014-11-06 08:48:26','email password'),(19,1,1,19,'2014-11-06 08:48:26','2014-11-06 08:48:26','recover username'),(20,1,1,20,'2014-11-06 08:48:26','2014-11-06 08:48:26','submit'),(21,1,1,21,'2014-11-06 08:48:26','2014-11-06 08:48:26','Sign in using your account with'),(22,1,1,22,'2014-11-06 08:48:26','2014-11-06 08:48:26','SignUp'),(23,1,1,23,'2014-11-06 08:48:26','2014-11-06 08:48:26','The username can contain uppercase/lowercase letters, numbers, and the special chars (-, _) only. The length should be between 3 to 15 characters. '),(24,1,1,24,'2014-11-06 08:48:26','2014-11-06 08:48:26','Username'),(25,1,1,25,'2014-11-06 08:48:26','2014-11-06 08:48:26','The password length must be longer than four characters, have both uppercase and lowercase characters, one numeric, and one or more of the following special characters (!@#$%^&*).	'),(26,1,1,26,'2014-11-06 08:48:26','2014-11-06 08:48:26','Password'),(27,1,1,27,'2014-11-06 08:48:26','2014-11-06 08:48:26','Confirm Password'),(28,1,1,28,'2014-11-06 08:48:26','2014-11-06 08:48:26','Full Name'),(29,1,1,29,'2014-11-06 08:48:26','2014-11-06 08:48:26','Email'),(30,1,1,30,'2014-11-06 08:48:26','2014-11-06 08:48:26','Affiliation'),(31,1,1,31,'2014-11-06 08:48:26','2014-11-06 08:48:26','submit'),(32,1,1,32,'2014-11-06 08:48:26','2014-11-06 08:48:26','Please note that even after registration, you will not be able to access until a moderator has authorized the login.');
/*!40000 ALTER TABLE `navigation_menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `places`
--

DROP TABLE IF EXISTS `places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `places` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `places`
--

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` VALUES (9,'Bottom'),(10,'BottomFigure'),(8,'BottomTitle'),(15,'Error'),(16,'ErrorMessage'),(12,'Foot'),(13,'FootBottom'),(11,'FootTitle'),(7,'Left'),(6,'LeftTitle'),(14,'Register'),(17,'Registered'),(5,'Right'),(4,'RightTitle'),(1,'Title'),(3,'Top'),(2,'TopTitle');
/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `registered_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_admin` int(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'administrator','eea9e76ef75508acddd1b5095839e7ce38216071','2014-11-06 08:43:13',1),(2,'fujita','eea9e76ef75508acddd1b5095839e7ce38216071','2014-11-06 08:43:13',0),(3,'takahashi','eea9e76ef75508acddd1b5095839e7ce38216071','2014-11-06 08:43:13',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `view_names`
--

DROP TABLE IF EXISTS `view_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `view_names` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cont_id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cont_id` (`cont_id`,`name`),
  CONSTRAINT `fk_view_1` FOREIGN KEY (`cont_id`) REFERENCES `controller_names` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `view_names`
--

LOCK TABLES `view_names` WRITE;
/*!40000 ALTER TABLE `view_names` DISABLE KEYS */;
INSERT INTO `view_names` VALUES (3,1,'glycans_facet'),(1,1,'json/glycans_json'),(2,1,'json/glycans_json_template'),(4,2,'list_all'),(5,2,'motifs'),(6,2,'motifs_list'),(7,2,'motifs_search'),(8,2,'motifs_tag'),(9,2,'results'),(10,2,'search'),(11,3,'index'),(12,4,'complete'),(13,4,'confirmation'),(14,4,'create'),(15,4,'graphical'),(16,4,'index'),(17,4,'motif'),(18,4,'newMotif'),(19,4,'no_data'),(20,4,'result'),(21,4,'upload'),(22,5,'check'),(23,5,'check_form'),(24,5,'glycan'),(25,5,'glycans'),(26,5,'glycans_list'),(27,5,'index'),(28,5,'insert'),(29,5,'interaction'),(30,5,'no_data'),(31,5,'registry'),(32,5,'result'),(33,5,'search_by_substructure'),(34,5,'substructure'),(35,6,'check'),(36,6,'check_form'),(37,6,'composition_search'),(38,6,'exact'),(39,6,'glycans'),(40,6,'glycans_list'),(41,6,'graphical'),(42,6,'motif'),(43,6,'search_by_substructure'),(44,6,'search_comp'),(45,6,'search_exact'),(46,6,'search_results'),(47,6,'search_sub'),(48,6,'structure_search'),(49,6,'sub'),(50,6,'substructure'),(51,7,'complete'),(52,7,'confirmation'),(53,7,'editpassword'),(54,7,'emailpw'),(55,7,'emailpwform'),(56,7,'index'),(57,7,'list_all'),(58,7,'no_data'),(59,7,'profile'),(60,7,'recover'),(61,7,'recoveruser'),(62,7,'recoveruserform'),(63,7,'registry'),(64,7,'result'),(65,7,'up');
/*!40000 ALTER TABLE `view_names` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-06 18:24:35
